package com.android.c88.acplay;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.android.c88.acplay.utils.NetworkObservable;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by HiepLH2 on 12/13/2017.
 */

public class ACCustomFragment extends Fragment implements Observer {



    @Override
    public void onResume() {
        super.onResume();
        Global.isConnected.addObserver(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Global.isConnected.deleteObserver(this);
    }

    void networkChanged(boolean isConnected) {}

    @Override
    public void update(Observable observable, Object o) {
        boolean isConnected = ((NetworkObservable)observable).isConnected();
        networkChanged(isConnected);
        Toast.makeText(ACCustomFragment.this.getActivity(), "network changed", Toast.LENGTH_LONG).show();
    }
}
