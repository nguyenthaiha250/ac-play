package com.android.c88.acplay;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.c88.acplay.activity.ActivityFragment;
import com.android.c88.acplay.activity.ActivityViewModel;
import com.android.c88.acplay.data.models.User;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.home.HomeFragment;
import com.android.c88.acplay.home.HomeViewModel;
import com.android.c88.acplay.library.LibraryContainerFragment;
import com.android.c88.acplay.library.LibraryFragment;
import com.android.c88.acplay.library.download.DownloadFragment;
import com.android.c88.acplay.library.download.DownloadViewModel;
import com.android.c88.acplay.library.history.HistoryFragment;
import com.android.c88.acplay.library.history.HistoryViewModel;
import com.android.c88.acplay.library.watchlater.WatchLaterFragment;
import com.android.c88.acplay.library.watchlater.WatchLaterViewModel;
import com.android.c88.acplay.login.LoginActivity;
import com.android.c88.acplay.search.SearchActivity;
import com.android.c88.acplay.subscription.SubscribedFragment;
import com.android.c88.acplay.subscription.SubscribedViewModel;
import com.android.c88.acplay.subscription.SubscriptionFragment;
import com.android.c88.acplay.subscription.SuggestionFragment;
import com.android.c88.acplay.subscription.SuggestionViewModel;
import com.android.c88.acplay.trend.TrendFragment;
import com.android.c88.acplay.trend.TrendViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import com.android.c88.acplay.utils.Utils;


import java.util.ArrayList;
import java.util.Date;

import static com.android.c88.acplay.data.source.remote.FirebaseInit.mAuth;

public class ACMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TrendFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        SubscriptionFragment.OnFragmentInteractionListener,
        SubscribedFragment.OnFragmentInteractionListener, SuggestionFragment.OnFragmentInteractionListener, LibraryContainerFragment.OnClickItemListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView imAvatar, imSearch, imLogo;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;
    private Dialog dialog;
    private TextView txtUserName, txtUserEmail;
    private View headerNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ac_main);

        Init();
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        toggle.syncState();
        // coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main_content);
        navigationView.setNavigationItemSelectedListener(this);


        imAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });

        imSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ACMainActivity.this, SearchActivity.class));
            }
        });

        setupViewPager(viewPager);
        imLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });
        tabLayout.setupWithViewPager(viewPager);
        TabLayoutSelected();
        if(!Utils.checkConnection(this)){
            Toast.makeText(this, "Khong co network", Toast.LENGTH_SHORT).show();
            viewPager.setCurrentItem(4);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                TabLayoutUnselected();
                switch (position) {
                    case 0:
                        tabLayout.getTabAt(0).setIcon(resize(getResources().getDrawable(R.drawable.home_active_icon)));
                        break;
                    case 1:
                        tabLayout.getTabAt(1).setIcon(resize(getResources().getDrawable(R.drawable.trend_active_icon)));
                        break;
                    case 2:
                        tabLayout.getTabAt(2).setIcon(resize(getResources().getDrawable(R.drawable.like_active_icon)));
                        break;
                    case 3:
                        tabLayout.getTabAt(3).setIcon(resize(getResources().getDrawable(R.drawable.notif_active_icon)));
                        break;
                    case 4:
                        tabLayout.getTabAt(4).setIcon(resize(getResources().getDrawable(R.drawable.lib_sound_icon)));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (mAuth.getCurrentUser() != null) {
            createDefaultProfile();
            FirebaseInit.mDatabase.getReference("User").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null) {
                            txtUserName.setText(user.getFullname());
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            if (FirebaseInit.mAuth.getCurrentUser() != null) {
                FirebaseInit.mDatabase.getReference("User").child(FirebaseInit.mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        txtUserName.setText(user.getFullname());
                        txtUserEmail.setText(user.get_id());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                txtUserEmail.setText(mAuth.getCurrentUser().getEmail());
            } else {
                txtUserName.setText("Guest");
                txtUserEmail.setText("loginplease@gmail.com");
            }

        }
    }

    public void createDefaultProfile() {
        if (mAuth.getCurrentUser() != null) {
            FirebaseInit.mDatabase.getReference("User").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user == null) {
                            Toast.makeText(getApplicationContext(), "user null", Toast.LENGTH_SHORT).show();
                            User first = new User();
                            first.set_id(mAuth.getCurrentUser().getUid());
                            first.setAge(20);
                            first.setAvatar("https://www.gstatic.com/mobilesdk/160503_mobilesdk/logo/2x/firebase_28dp.png");
                            first.setFullname("Mr. " + new Date().getSeconds());
                            ArrayList<String> list = new ArrayList<>();
                            for (int i = 0; i < 7; i++) {
                                list.add("null");
                            }
                            first.setCategory_id(list);
                            FirebaseInit.mDatabase.getReference("User").child(mAuth.getCurrentUser().getUid()).setValue(first).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "create profile success", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), String.valueOf(e), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "user != null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "data null", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), String.valueOf(databaseError), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public void Init() {
        navigationView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        imAvatar = findViewById(R.id.avatar_image);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        imSearch = findViewById(R.id.iv_search);
        imLogo = findViewById(R.id.iv_logo);
        headerNavView = navigationView.getHeaderView(0);
        txtUserName = headerNavView.findViewById(R.id.txt_user_name);
        txtUserEmail = headerNavView.findViewById(R.id.txt_user_email);
    }

    public void TabLayoutSelected() {
        tabLayout.getTabAt(0).setIcon(resize(getResources().getDrawable(R.drawable.home_active_icon)));
        tabLayout.getTabAt(1).setIcon(resize(getResources().getDrawable(R.drawable.trend_icon)));
        tabLayout.getTabAt(2).setIcon(resize(getResources().getDrawable(R.drawable.like_icon)));
        tabLayout.getTabAt(3).setIcon(resize(getResources().getDrawable(R.drawable.notif_icon)));
        tabLayout.getTabAt(4).setIcon(resize(getResources().getDrawable(R.drawable.lib_icon)));
    }

    public void TabLayoutUnselected() {
        tabLayout.getTabAt(0).setIcon(resize(getResources().getDrawable(R.drawable.home_icon)));
        tabLayout.getTabAt(1).setIcon(resize(getResources().getDrawable(R.drawable.trend_icon)));
        tabLayout.getTabAt(2).setIcon(resize(getResources().getDrawable(R.drawable.like_icon)));
        tabLayout.getTabAt(3).setIcon(resize(getResources().getDrawable(R.drawable.notif_icon)));
        tabLayout.getTabAt(4).setIcon(resize(getResources().getDrawable(R.drawable.lib_icon)));
    }

    public static HomeViewModel obtainHomeViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        HomeViewModel viewModel = ViewModelProviders.of(activity, factory).get(HomeViewModel.class);

        return viewModel;
    }
    public static DownloadViewModel obtainDownloadViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        DownloadViewModel viewModel = ViewModelProviders.of(activity, factory).get(DownloadViewModel.class);

        return viewModel;
    }

    public static WatchLaterViewModel obtainWatchLaterViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        WatchLaterViewModel viewModel = ViewModelProviders.of(activity, factory).get(WatchLaterViewModel.class);

        return viewModel;
    }

    public static ActivityViewModel obtainActivityViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        ActivityViewModel viewModel = ViewModelProviders.of(activity, factory).get(ActivityViewModel.class);

        return viewModel;
    }

    public static TrendViewModel obtainTrendViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        TrendViewModel viewModel = ViewModelProviders.of(activity, factory).get(TrendViewModel.class);

        return viewModel;
    }

    public static HistoryViewModel obtainHistoryViewmodel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        HistoryViewModel viewModel = ViewModelProviders.of(activity, factory).get(HistoryViewModel.class);

        return viewModel;
    }

    public static SubscribedViewModel obtainSubscribedViewmodel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        SubscribedViewModel viewModel = ViewModelProviders.of(activity, factory).get(SubscribedViewModel.class);
        return viewModel;
    }

    public static SuggestionViewModel obtainSuggestionViewmodel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        SuggestionViewModel viewModel = ViewModelProviders.of(activity, factory).get(SuggestionViewModel.class);
        return viewModel;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HomeFragment.newInstance());
        adapter.addFragment(TrendFragment.newInstance());
        adapter.addFragment(SubscriptionFragment.newInstance());
        adapter.addFragment(ActivityFragment.newInstance());
        adapter.addFragment(new LibraryFragment());
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onClicked(int position) {
        switch (position) {
            case 1:
                HistoryFragment historyFragment = new HistoryFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_library_container, historyFragment).addToBackStack(null).commit();
                break;
            case 2:
                WatchLaterFragment watchLaterFragment = new WatchLaterFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_library_container, watchLaterFragment).addToBackStack(null).commit();
                break;
            case 3:
                DownloadFragment downloadFragment = new DownloadFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_library_container, downloadFragment).addToBackStack(null).commit();
                break;
            case 4:
                Snackbar.make(drawer, "Please access internet", Snackbar.LENGTH_INDEFINITE).setAction("Retry",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(ACMainActivity.this, "cleckd", Toast.LENGTH_LONG).show();
                            }
                        }).show();
        }
    }

    private void onclickBackButton() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this,"clicked",Toast.LENGTH_LONG).show();
                int i = viewPager.getCurrentItem();
                /*Toast.makeText(MainActivity.this,"clicked" +i,Toast.LENGTH_LONG).show();
                mSectionsPagerAdapter.getItem(i);*/
                if (i == 3) {
                    LibraryFragment libraryFragment = new LibraryFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fl_library, libraryFragment).addToBackStack(null).commit();
                    toolbar.setTitle(R.string.app_name);
                    toolbar.setNavigationIcon(null);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        int curentPage = viewPager.getCurrentItem();
        if (curentPage != 4) {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_switch_account) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure want to logout?");
            builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mAuth.signOut();
                    Toast.makeText(ACMainActivity.this, "Hihihi", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ACMainActivity.this, LoginActivity.class));
                    finish();
                }
            });
            builder.create().show();
        } else if (id == R.id.nav_term) {
            Toast.makeText(this, "Terms and policies", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_setting) {
            Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_share) {
            Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_help) {
            Toast.makeText(this, "Help and feedback", Toast.LENGTH_SHORT).show();
        }
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 600, 600, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    /*@Override
    protected void onPause() {
        super.onPause();
        ACPlayApplication.activityPaused();// On Pause notify the Application
    }

    @Override
    protected void onResume() {
        super.onResume();
        ACPlayApplication.activityResumed();// On Resume notify the Application
    }*/

    public void setItemChangedInternet(boolean isConnected){
        LibraryContainerFragment libraryContainerFragment=(LibraryContainerFragment)getSupportFragmentManager().findFragmentById(R.id.fl_library_container);
        if (isConnected) {
                  if(libraryContainerFragment!=null){
                libraryContainerFragment.onlineStyle();
            }
        } else {
            if(libraryContainerFragment!=null){
                libraryContainerFragment.offlineStyle();
            }
        }
    }

}
