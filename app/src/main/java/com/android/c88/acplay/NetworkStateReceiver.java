package com.android.c88.acplay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.c88.acplay.utils.Utils;

/**
 * Created by HaNT58 on 12/11/2017.
 */

public class NetworkStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Utils.checkConnection(context)) {
            Global.isConnected.setConnected(true);
        } else {
            Global.isConnected.setConnected(false);
        }

    }
}
