package com.android.c88.acplay;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.VisibleForTesting;

import com.android.c88.acplay.activity.ActivityViewModel;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.source.local.DownloadData;
import com.android.c88.acplay.data.source.local.GuestData;
import com.android.c88.acplay.data.source.local.LocalDataSource;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.data.source.remote.RemoteDataSource;
import com.android.c88.acplay.home.HomeViewModel;
import com.android.c88.acplay.library.download.DownloadViewModel;
import com.android.c88.acplay.library.history.HistoryViewModel;
import com.android.c88.acplay.library.watchlater.WatchLaterViewModel;
import com.android.c88.acplay.media.VideoRelatedViewModel;
import com.android.c88.acplay.subscription.SubscribedViewModel;
import com.android.c88.acplay.subscription.SuggestionViewModel;
import com.android.c88.acplay.trend.TrendViewModel;

import io.paperdb.Paper;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    @SuppressLint("StaticFieldLeak")
    private static volatile ViewModelFactory INSTANCE;

    private final Application mApplication;

    private final Repository mRepository;

    public static ViewModelFactory getInstance(Application application) {

        if (INSTANCE == null) {
            synchronized (ViewModelFactory.class) {
                if (INSTANCE == null) {
                    //init remote data
                    RemoteDataSource remoteDataSource = new RemoteDataSource(new FirebaseInit());

                    // init local data
                    Paper.init(application);
                    GuestData guestData = new GuestData();
                    DownloadData downloadData = new DownloadData();
                    LocalDataSource localDataSource = new LocalDataSource(downloadData, guestData);

                    // init repository
                    Repository repository = new Repository(remoteDataSource, localDataSource);
                    INSTANCE = new ViewModelFactory(application, repository);
                }
            }
        }
        return INSTANCE;
    }

    @VisibleForTesting
    public static void destroyInstance() {
        INSTANCE = null;
    }

    private ViewModelFactory(Application application, Repository repository) {
        mApplication = application;
        mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            //noinspection unchecked
            return (T) new HomeViewModel(mApplication, mRepository);
        }else if (modelClass.isAssignableFrom(SubscribedViewModel.class)){
            return (T) new SubscribedViewModel(mApplication, mRepository);
        }
        else if (modelClass.isAssignableFrom(SuggestionViewModel.class)){
            return (T) new SuggestionViewModel(mApplication,mRepository);
        }else if(modelClass.isAssignableFrom(TrendViewModel.class)){
            return (T) new TrendViewModel(mApplication, mRepository);
        }
        else if(modelClass.isAssignableFrom(HistoryViewModel.class)){
            return (T) new HistoryViewModel(mApplication, mRepository);
        }

        else if (modelClass.isAssignableFrom(VideoRelatedViewModel.class)){
            return (T) new VideoRelatedViewModel(mApplication, mRepository);
        }
        else if (modelClass.isAssignableFrom(ActivityViewModel.class)){
            return (T) new ActivityViewModel(mApplication, mRepository);
        }
        else if (modelClass.isAssignableFrom(WatchLaterViewModel.class)){
            return (T) new WatchLaterViewModel(mApplication, mRepository);
        }
        else if (modelClass.isAssignableFrom(DownloadViewModel.class)){
            return (T) new DownloadViewModel(mApplication, mRepository);
        }
//        else if (modelClass.isAssignableFrom(TaskDetailViewModel.class)) {
//            //noinspection unchecked
//            return (T) new TaskDetailViewModel(mApplication, mTasksRepository);
//        } else if (modelClass.isAssignableFrom(AddEditTaskViewModel.class)) {
//            //noinspection unchecked
//            return (T) new AddEditTaskViewModel(mApplication, mTasksRepository);
//        } else if (modelClass.isAssignableFrom(TasksViewModel.class)) {
//            //noinspection unchecked
//            return (T) new TasksViewModel(mApplication, mTasksRepository);
//        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
