package com.android.c88.acplay.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.library.download.DownloadAdapter;
import com.android.c88.acplay.library.download.DownloadViewModel;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class ActivityAdapter extends BaseAdapter {
    Context context;

    List<Notification> notifications;
    ActivityViewModel mActivityViewModel;

    public ActivityAdapter(Context context, List<Notification> notifications, ActivityViewModel model) {
        this.context = context;
        setNotifications(notifications);
        mActivityViewModel = model;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ActivityAdapter.ViewHolder viewHolder;
        if (view==null){
            LayoutInflater inflater = LayoutInflater.from(context);
            view=inflater.inflate(R.layout.activity_item,null);
            viewHolder = new ActivityAdapter.ViewHolder();
            viewHolder.ivThumbnals = view.findViewById(R.id.iv_activity_thumbnals);
            viewHolder.tvContent = view.findViewById(R.id.tv_activity_content);
            viewHolder.tvDate=view.findViewById(R.id.tv_activity_date);

            view.setTag(viewHolder);
        }else viewHolder = (ActivityAdapter.ViewHolder) view.getTag();
        Notification notification=notifications.get(i);
        viewHolder.tvContent.setText(notification.getContent());
        Date date = new Date(notification.getDate());
        SimpleDateFormat df = new SimpleDateFormat("hh:mm dd/MM/yyyy");
        viewHolder.tvDate.setText(df.format(date));
        switch (notification.getCategory_id()){
            case "film-hai":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_funny));
                break;
            case "film-hanhdong":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action));
                break;
            case "film-hoathinh":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_animation));
                break;
            case "film-kinhdi":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_horror));
                break;
            case "film-mv":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_mv));
                break;
            case "film-tinhcam":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_love));
                break;
            case "film-vientuong":
                viewHolder.ivThumbnals.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_vientuong));
                break;
        }
        /*Picasso.with(context)
                .load(notification.getThumnails())
                .into(viewHolder.ivThumbnals);*/



        return view;
    }
    public class ViewHolder {
        ImageView ivThumbnals;
        TextView tvContent,tvDate;
    }
}
