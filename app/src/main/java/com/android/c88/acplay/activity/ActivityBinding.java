package com.android.c88.acplay.activity;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.library.download.DownloadAdapter;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class ActivityBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:itemsActivity")
    public static void setItems(ListView listView, List<Notification> items) {
        ActivityAdapter adapter = (ActivityAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.setNotifications(items);
        }
    }
}
