package com.android.c88.acplay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.c88.acplay.ACCustomFragment;
import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.databinding.FragmentActivityBinding;
import com.android.c88.acplay.media.VideoViewActivity;

import java.io.Serializable;
import java.util.ArrayList;


public class ActivityFragment extends ACCustomFragment {

    private static String TAG = "ACPlayHF";
    private FragmentActivityBinding mFragmentActivityBinding;
    private ActivityViewModel mActivityViewModel;
    private ActivityAdapter adapter;
    private Intent intent;

    public ActivityFragment() {
    }

    public static ActivityFragment newInstance() {
        return  new ActivityFragment();
    }
    @Override
    public void onResume() {
        super.onResume();
        mActivityViewModel.start();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentActivityBinding = FragmentActivityBinding.inflate(inflater, container, false);
        mActivityViewModel = ACMainActivity.obtainActivityViewmodel(getActivity());
        mFragmentActivityBinding.setViewmodel(mActivityViewModel);
        adapter = new ActivityAdapter(getContext(), new ArrayList<Notification>(), mActivityViewModel);
        mFragmentActivityBinding.lvActivity.setAdapter(adapter);

        /*LinearLayout.LayoutParams lvVideoLParam = (LinearLayout.LayoutParams) mFragmentHomeBinding.lvVideo.getLayoutParams();
        lvVideoLParam.width = (int) 0.95f*ACMainActivity.SC_WIDTH;
        mFragmentHomeBinding.lvVideo.setLayoutParams(lvVideoLParam);*/

        mFragmentActivityBinding.lvActivity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), mActivityViewModel.listNotifications.get(i).getContent(), Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoViewActivity.KEY_MOVIE, (Serializable) mActivityViewModel.listNotifications);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return mFragmentActivityBinding.getRoot();
        //return inflater.inflate(R.layout.fragment_activity, container, false);
    }

}
