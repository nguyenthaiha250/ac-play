package com.android.c88.acplay.activity;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.home.HomeViewModel;
import com.google.firebase.database.DatabaseError;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class ActivityViewModel extends AndroidViewModel {

    private static String TAG = "ACPlayHVM";
    private Repository mRepository;
    public ObservableList<Notification> listNotifications = new ObservableArrayList<>();

    public ActivityViewModel(@NonNull Application application, Repository repository) {
        super(application);
        mRepository = repository;
    }

    public void start() {
        getNotification();
    }

    void getNotification() {
        listNotifications.clear();

        mRepository.getAllNotification(new DataSource.GetAllNotificationCallback() {
            @Override
            public void onLoaded(List<Notification> notifications) {
                ActivityViewModel.this.listNotifications.addAll(notifications);
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.d(TAG, "Load Notification Error");
            }
        });
    }
}
