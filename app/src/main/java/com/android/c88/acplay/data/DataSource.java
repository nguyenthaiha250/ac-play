package com.android.c88.acplay.data;

import com.android.c88.acplay.data.models.Category;
import com.android.c88.acplay.data.models.History;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.google.firebase.database.DatabaseError;

import java.util.HashMap;
import java.util.List;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public interface DataSource {
    void getMovie(AddMovieCallback callback);

    void getMovies(GetMoviesCallback callback);

    void getSubsCategory(GetSubsCategoryCallback callback);

    void getSuggCategory(GetSuggCategoryCallback callback);

    void getHistory(GetHistoryCallback callback);

    void getWatchLater(GetWatchLaterCallback callback);

    void getTrend(GetTrendCallback callback);

    void getAllNotification(GetAllNotificationCallback callback);

    void signInWithGuest(SignInWithGuestCallback callback);

    void isGuest(IsGuestCallback callback);

    void createProfileUser(CreateProfileUserCallback callback);

    void getHistoriesOnline(GetHistoriesCallback callback);

    void pushHistories(Movie movie);

    void pushNotification(Notification notification);


    interface GetHistoriesCallback{
        void onLoaded(List<History> histories);

        void onCancel(DatabaseError error);
    }

    interface CreateProfileUserCallback{
        void onSuccess();
        void onFailure(Exception e);
    }

    interface IsGuestCallback{
        void isTrue(boolean isTrue);
    }

    interface SignInWithGuestCallback{
        void onSuccess();
        void onFailure(Exception error);
    }

    interface GetAllNotificationCallback{
        void onLoaded(List<Notification> notifications);

        void onCancel(DatabaseError error);
    }
    void getDownload(GetDownloadCallback callback);

    void getVideoRelated(String id, GetVideoRelatedCallBack callBack);

    interface GetVideoRelatedCallBack {
        void onLoaded(List<Movie> movies);

        void onCancel(DatabaseError error);
    }

    interface GetDownloadCallback{
        void onLoaded(List<Movie> movies);

        void onCancel(DatabaseError error);
    }



    void subscribeChannel(SubscribeChannelCallback callback, String s);

    void unsubscribeChannel(UnsubscribeChannelCallback callback, String s);


    interface AddMovieCallback {
        void movieAdded(Movie movie);
    }

    interface GetMoviesCallback {
        void onLoaded(List<Movie> movies);

        void onCancel(DatabaseError error);
    }

    interface GetSubsCategoryCallback {
        void onLoaded(List<Category> categories);

        void onCancel(DatabaseError error);
    }

    interface GetSuggCategoryCallback {
        void onLoaded(List<Category> categories);

        void onCancel(DatabaseError error);
    }

    interface GetHistoryCallback {
        void onLoaded(HashMap<Long, Movie> histories);

        void onCancel(DatabaseError error);
    }

    interface GetWatchLaterCallback {
        void onLoaded(List<Movie> videoWatchLater);

        void onCancel(DatabaseError error);
    }

    interface GetTrendCallback {
        void onLoaded(List<Movie> movies);

        void onCancel(DatabaseError error);
    }

    interface UnsubscribeChannelCallback {
        void onSuccess(Void aVoid);

        void onFailture(Exception e);
    }

    interface SubscribeChannelCallback {
        void onSuccess(Void aVoid);

        void onFailture(Exception e);
    }

    // Note: add some Movie to download Lib
    void addMovieToDownload(Movie movie);

    // Note: add some movie to History
    void addMovieToHistory(long time,Movie movie);

    // Note: add some movie to WatchLater
    void addMovieToWatchLater(Movie movie);


}
