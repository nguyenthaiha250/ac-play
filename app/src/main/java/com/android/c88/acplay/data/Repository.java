package com.android.c88.acplay.data;

import android.util.Log;

import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.data.source.local.LocalDataSource;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.data.source.remote.RemoteDataSource;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public class Repository implements DataSource {
    RemoteDataSource mRemoteDataSource;
    LocalDataSource mLocalDataSource;

    public Repository(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        this.mRemoteDataSource = remoteDataSource;
        this.mLocalDataSource = localDataSource;
    }

    @Override
    public void getMovie(AddMovieCallback callback) {
        mRemoteDataSource.getMovie(callback);
    }

    @Override
    public void getMovies(GetMoviesCallback callback) {
        mRemoteDataSource.getMovies(callback);
    }


    @Override
    public void getSubsCategory(GetSubsCategoryCallback callback) {
        if(FirebaseInit.mAuth.getCurrentUser()!=null){
            mRemoteDataSource.getSubsCategory(callback);
        }
    }

    @Override
    public void getSuggCategory(GetSuggCategoryCallback callback) {
        if(FirebaseInit.mAuth.getCurrentUser()!=null){
            mRemoteDataSource.getSuggCategory(callback);
        }
    }

    @Override
    public void getHistory(GetHistoryCallback callback) {
        mLocalDataSource.getHistory(callback);
    }

    @Override
    public void getWatchLater(GetWatchLaterCallback callback) {
        mLocalDataSource.getWatchLater(callback);
    }

    @Override
    public void getTrend(GetTrendCallback callback) {
        mRemoteDataSource.getTrend(callback);
    }

    @Override
    public void getDownload(GetDownloadCallback callback) {
        mLocalDataSource.getDownload(callback);
    }

    @Override
    public void getVideoRelated(String id, GetVideoRelatedCallBack callBack) {
        mRemoteDataSource.getVideoRelated(id, callBack);
    }

    @Override
    public void getAllNotification(GetAllNotificationCallback callback) {
        mRemoteDataSource.getAllNotification(callback);
    }

    @Override
    public void signInWithGuest(SignInWithGuestCallback callback) {
        Log.e("login","3");
        mRemoteDataSource.signInWithGuest(callback);

    }

    @Override
    public void isGuest(IsGuestCallback callback) {
        mRemoteDataSource.isGuest(callback);
    }

    @Override
    public void createProfileUser(CreateProfileUserCallback callback) {
        mRemoteDataSource.createProfileUser(callback);
    }

    @Override
    public void getHistoriesOnline(GetHistoriesCallback callback) {
        mRemoteDataSource.getHistoriesOnline(callback);
    }

    @Override
    public void pushHistories(Movie movie) {
        if(FirebaseInit.mAuth.getCurrentUser()!=null){
            mRemoteDataSource.pushHistories(movie);
        }else{
            mLocalDataSource.pushHistories(movie);
        }
    }

    @Override
    public void pushNotification(Notification notification) {
        if(FirebaseInit.mAuth.getCurrentUser()!=null){
            mRemoteDataSource.pushNotification(notification);
        }else{
            mLocalDataSource.pushNotification(notification);
        }
    }


    public void subscribeChannel(SubscribeChannelCallback callback, String s) {
        mRemoteDataSource.subscribeChannel(callback, s);
    }

    @Override
    public void unsubscribeChannel(UnsubscribeChannelCallback callback, String s) {
        mRemoteDataSource.unsubscribeChannel(callback, s);
    }

    // Add movie to download Repo
    @Override
    public void addMovieToDownload(Movie movie) {
        mLocalDataSource.addMovieToDownload(movie);
    }
    // Add movie to History Repo
    @Override
    public void addMovieToHistory(long time,Movie movie) {

    }
    // Add movie to WatchLater Repo
    @Override
    public void addMovieToWatchLater(Movie movie) {

    }


}
