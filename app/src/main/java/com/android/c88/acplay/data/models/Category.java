package com.android.c88.acplay.data.models;

import java.util.ArrayList;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class Category {
    private String _id, thumnails, title;
    private ArrayList<String> user_id;

    public Category(String _id, String thumnails, String title, ArrayList<String> user_id) {
        this._id = _id;
        this.thumnails = thumnails;
        this.title = title;
        this.user_id = user_id;
    }

    public Category() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getThumnails() {
        return thumnails;
    }

    public void setThumnails(String thumnails) {
        this.thumnails = thumnails;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getUser_id() {
        return user_id;
    }

    public void setUser_id(ArrayList<String> user_id) {
        this.user_id = user_id;
    }
}
