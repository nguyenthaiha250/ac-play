package com.android.c88.acplay.data.models;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class History {
    private String _id, movie_id, title;
    private long date;

    public History(String _id, String movie_id, String title, long date) {
        this._id = _id;
        this.movie_id = movie_id;
        this.title = title;
        this.date = date;
    }

    public History() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(String movie_id) {
        this.movie_id = movie_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
