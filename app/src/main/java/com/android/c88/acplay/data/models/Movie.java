package com.android.c88.acplay.data.models;

import java.io.Serializable;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class Movie implements Serializable {
    private String _id, title,category_id, categoryName, thumnails, src, description;
    private int view;
    private long duration, date;

    public Movie(String _id, String title, String category_id, String categoryName, String thumnails, String src, String description, int view, long duration, long date) {
        this._id = _id;
        this.title = title;
        this.category_id = category_id;
        this.categoryName = categoryName;
        this.thumnails = thumnails;
        this.src = src;
        this.description = description;
        this.view = view;
        this.duration = duration;
        this.date = date;
    }

    @Override
    public boolean equals(Object obj) {
        return ((Movie)obj)._id.equals(this._id)&&((Movie)obj)._id.equals(this._id)&&
                ((Movie)obj).title.equals(this.title)&&
                ((Movie)obj).thumnails.equals(this.thumnails)&&
                ((Movie)obj).src.equals(this.src)&&
                ((Movie)obj).description.equals(this.description);
    }

    public Movie() {
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getThumnails() {
        return thumnails;
    }

    public void setThumnails(String thumnails) {
        this.thumnails = thumnails;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
