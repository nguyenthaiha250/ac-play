package com.android.c88.acplay.data.models;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class Notification {
    private String _id, user_id, content, category_id;
    private long date;

    public Notification(String _id, String user_id, String content, String category_id, long date) {
        this._id = _id;
        this.user_id = user_id;
        this.content = content;
        this.category_id = category_id;
        this.date = date;
    }

    public Notification() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
