package com.android.c88.acplay.data.models;

import java.util.ArrayList;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class User {
    private String _id, avatar, fullname;
    private ArrayList<String> category_id;
    private int age;

    public User(String _id, String avatar, String fullname, ArrayList<String> category_id, int age) {
        this._id = _id;
        this.avatar = avatar;
        this.fullname = fullname;
        this.category_id = category_id;
        this.age = age;
    }

    public User() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public ArrayList<String> getCategory_id() {
        return category_id;
    }

    public void setCategory_id(ArrayList<String> category_id) {
        this.category_id = category_id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
