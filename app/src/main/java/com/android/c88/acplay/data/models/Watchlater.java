package com.android.c88.acplay.data.models;

import java.util.ArrayList;

/**
 * Created by NhanNT25 on 12/4/2017.
 */

public class Watchlater {
    private String _id;
    private ArrayList<String> movie_id;
    private long date;

    public Watchlater(String _id, ArrayList<String> movie_id, long date) {
        this._id = _id;
        this.movie_id = movie_id;
        this.date = date;
    }

    public Watchlater() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<String> getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(ArrayList<String> movie_id) {
        this.movie_id = movie_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
