package com.android.c88.acplay.data.source.local;

import android.content.Context;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.models.Movie;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

/**
 * Created by NguyenNV6 on 12/11/2017.
 */

public class DownloadData implements IPaperData {

    public List<Movie> getMoviesDownload() {
        List<Movie> result = new ArrayList<>();
        if (Paper.book().read(download) != null) result = Paper.book().read(download);
        return result;
    }

    public void getMoviesDownload(final DataSource.GetDownloadCallback callback) {
        List<Movie> moviesDownload = getMoviesDownload();
        callback.onLoaded(moviesDownload);
    }


    public void addVideoDownloadToData(Movie movie) {
        List<Movie> moviesDownload = getMoviesDownload();
        moviesDownload.add(movie);
        Paper.book().write(download, moviesDownload);
    }

    public Movie getMovie(Movie movie) {
        List<Movie> moviess = getMoviesDownload();
        for (Movie m : moviess) {
            if (movie.equals(m))
                return m;
        }
        return null;
    }

    public void deleteVideoDownload(Movie movie) {
        List<Movie> moviesDownload = getMoviesDownload();
        moviesDownload.remove(movie);
        Paper.book().write(download, moviesDownload);
    }
}
