package com.android.c88.acplay.data.source.local;

import android.content.Context;
import android.provider.MediaStore;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Watchlater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.paperdb.Paper;

/**
 * Created by NguyenNV6 on 12/4/2017.
 */

public class GuestData implements IPaperData {

    //load data from paper then return the list video watched
    public HashMap<Long, Movie> getListVideoHistory() {
        return Paper.book().read(history, new HashMap<Long, Movie>());
    }
    // load data list video watchlater
    public List<Movie> getListVideoWatchLater() {
        return Paper.book().read(watchlater, new ArrayList<Movie>());
    }

    public void getListVideoHistory(DataSource.GetHistoryCallback callback) {
        HashMap<Long, Movie> listHistory = getListVideoHistory();
        callback.onLoaded(listHistory);
    }

    public void getListVideoWatchLater(DataSource.GetWatchLaterCallback callback) {
        List<Movie> listWatchLater = getListVideoWatchLater();
        callback.onLoaded(listWatchLater);
    }

    public void addVideoToHistory(long time, Movie video) {
        HashMap<Long, Movie> listVideoHistory = getListVideoHistory();
        listVideoHistory.put(time, video);
        Paper.book().write(history, listVideoHistory);
    }


    public void addVideoToWatchLater(Movie video) {
        List<Movie> listVideoWatchLater = getListVideoWatchLater();
        listVideoWatchLater.add(video);
        Paper.book().write(watchlater, listVideoWatchLater);
    }

    //delete all history
    public void deleteHistory() {
        Paper.book().delete(history);
    }

    //delete all video watchLater
    public void deleteWatchLater() {
        Paper.book().delete(watchlater);
    }
}
