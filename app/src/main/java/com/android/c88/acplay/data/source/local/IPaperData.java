package com.android.c88.acplay.data.source.local;

/**
 * Created by NguyenNV6 on 12/12/2017.
 */

public interface IPaperData {
    String download = "DOWNLOAD";
    String history = "HISTORY";
    String watchlater = "WATCHLATER";
}
