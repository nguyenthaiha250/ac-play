package com.android.c88.acplay.data.source.local;

import android.os.Environment;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.models.Category;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;

import java.util.List;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public class LocalDataSource implements DataSource {
    DownloadData mDownloadData;
    GuestData mGuestData;

    public LocalDataSource(DownloadData mDownloadData, GuestData mGuestData) {
        this.mDownloadData = mDownloadData;
        this.mGuestData = mGuestData;
        mGuestData.addVideoToWatchLater(new Movie("454DSD", "Khi Cô Đơn Em Nhớ Ai", "DEMO", "DEMO", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Pictures/", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Movies/aa.mp4", "demo thoi ma", 1, 12412, 214142));
        mGuestData.addVideoToWatchLater(new Movie("454DSD", "Khi Cô Đơn Em Nhớ Ai", "DEMO", "DEMO", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Pictures/", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Movies/aa.mp4", "demo thoi ma", 1, 12412, 214142));
        mGuestData.addVideoToHistory(121345, new Movie("454DSD", "Khi Cô Đơn Em Nhớ Ai", "DEMO", "DEMO", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Pictures/", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Movies/aa.mp4", "demo thoi ma", 1, 12412, 214142));
        mGuestData.addVideoToHistory(121345, new Movie("454DSD", "Khi Cô Đơn Em Nhớ Ai", "DEMO", "DEMO", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Pictures/", "/storage/emulated/0/Android/data/com.android.c88.acplay/files/Movies/aa.mp4", "demo thoi ma", 1, 12412, 214142));
    }

    @Override
    public void getMovie(AddMovieCallback callback) {

    }

    @Override
    public void getMovies(GetMoviesCallback callback) {

    }

    @Override
    public void getSubsCategory(GetSubsCategoryCallback callback) {

    }

    @Override
    public void getSuggCategory(GetSuggCategoryCallback callback) {

    }

    @Override
    public void getHistory(GetHistoryCallback callback) {
        mGuestData.getListVideoHistory(callback);
    }

    @Override
    public void getWatchLater(GetWatchLaterCallback callback) {
        mGuestData.getListVideoWatchLater(callback);
    }

    @Override
    public void getTrend(GetTrendCallback callback) {

    }

    @Override
    public void getDownload(GetDownloadCallback callback) {
        mDownloadData.getMoviesDownload(callback);
    }

    @Override
    public void getVideoRelated(String id, GetVideoRelatedCallBack callBack) {

    }

    @Override

    public void getAllNotification(GetAllNotificationCallback callback) {

    }

    @Override
    public void signInWithGuest(SignInWithGuestCallback callback) {

    }

    @Override
    public void isGuest(IsGuestCallback callback) {
    }

    @Override
    public void createProfileUser(CreateProfileUserCallback callback) {

    }

    @Override
    public void getHistoriesOnline(GetHistoriesCallback callback) {

    }

    @Override
    public void pushHistories(Movie movie) {

    }

    @Override
    public void pushNotification(Notification notification) {

    }


    @Override
    public void subscribeChannel(SubscribeChannelCallback callback, String s) {

    }

    @Override
    public void unsubscribeChannel(UnsubscribeChannelCallback callback, String s) {

    }

    @Override
    public void addMovieToDownload(Movie movie) {
        mDownloadData.addVideoDownloadToData(movie);
    }

    @Override
    public void addMovieToHistory(long time, Movie movie) {
        mGuestData.addVideoToHistory(time, movie);
    }

    @Override
    public void addMovieToWatchLater(Movie movie) {
        mGuestData.addVideoToWatchLater(movie);
    }
}
