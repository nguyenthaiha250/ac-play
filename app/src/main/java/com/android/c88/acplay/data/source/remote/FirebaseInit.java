package com.android.c88.acplay.data.source.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.models.Category;
import com.android.c88.acplay.data.models.History;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;
import com.android.c88.acplay.data.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by HuuMX1 on 12/6/2017.
 */

public class FirebaseInit {
    private static String TAG = "FirebaseInit";
    public static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    public static FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    public static FirebaseStorage mStorage = FirebaseStorage.getInstance();




    public OnUpdateVideoListener onUpdateVideoListener;
    public OnUpdateSubscribedChannelListener onUpdateSubscribedChannelListener;
    public OnUpdateSuggestionChannelListener onUpdateSuggestionChannelListener;
    public interface OnUpdateVideoListener{
        void getVideoEvent(Movie movie);
    }
    public void setOnUpdateVideoListener(OnUpdateVideoListener onUpdateVideoListener) {
        this.onUpdateVideoListener = onUpdateVideoListener;
    }

    public interface OnUpdateSubscribedChannelListener{
        void getSubscribedChannelEvent(Category category);
    }
    public void setOnUpdateSubscribedChannelListener(OnUpdateSubscribedChannelListener onUpdateSubscribedChannelListener) {
        this.onUpdateSubscribedChannelListener = onUpdateSubscribedChannelListener;
    }

    public interface OnUpdateSuggestionChannelListener{
        void getSuggestionChannelEvent(Category category);
    }
    public void setOnUpdateSuggestionChannelListener(OnUpdateSuggestionChannelListener onUpdateSuggestionChannelListener) {
        this.onUpdateSuggestionChannelListener = onUpdateSuggestionChannelListener;
    }


    public void isGuest(final DataSource.IsGuestCallback callback){
        if(mAuth.getCurrentUser().isAnonymous()){
            callback.isTrue(true);
        }else{
            callback.isTrue(false);
        }
    }
    public void signInWithGuest(final DataSource.SignInWithGuestCallback callback){
        FirebaseAuth.getInstance().signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    callback.onSuccess();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void getMovie(final DataSource.AddMovieCallback callback) {
        FirebaseInit.mDatabase.getReference("Movie").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, "onChildAdded");
                if(dataSnapshot!=null) {
                    Movie m = dataSnapshot.getValue(Movie.class);
                    callback.movieAdded(m);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, "onChildChanged");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e(TAG, "onChildRemoved");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, "onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getAllMovies(final DataSource.GetMoviesCallback callback) {
        FirebaseInit.mDatabase.getReference("Movie").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Firebase", "onDataChange:" + dataSnapshot.toString());
                List<Movie> movies = new ArrayList<>();
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot != null) {
                            Movie movie = postSnapshot.getValue(Movie.class);
                            movies.add(movie);
                        }
                    }
                    callback.onLoaded(movies);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel(databaseError);
            }
        });
    }

    public void getSubcribedChannel(final DataSource.GetSubsCategoryCallback callback) {
        final List<Category> list = new ArrayList<>();
        mDatabase.getReference("User").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null){
                    list.clear();
                    User user = dataSnapshot.getValue(User.class);
                    if(user!=null && user.getCategory_id()!=null){
                        ArrayList<String> listCate = user.getCategory_id();
                        int size = listCate.size();
                        for (int i = 0; i < size; i++) {
                            if(!listCate.get(i).equalsIgnoreCase("null")){
                                Category category = new Category();
                                category.set_id(listCate.get(i));

                                switch (listCate.get(i)){
                                    case "film-hai":
                                        category.setTitle("Phim hài");
                                        break;
                                    case "film-hanhdong":
                                        category.setTitle("Phim hành động");
                                        break;
                                    case "film-hoathinh":
                                        category.setTitle("Phim hoạt hình");
                                        break;
                                    case "film-kinhdi":
                                        category.setTitle("Phim kinh dị");
                                        break;
                                    case "film-mv":
                                        category.setTitle("Music Video");
                                        break;
                                    case "film-tinhcam":
                                        category.setTitle("Phim tình cảm");
                                        break;
                                    case "film-vientuong":
                                        category.setTitle("Phim viễn tưởng");
                                        break;
                                }
                                category.setThumnails(null);
                                category.setUser_id(null);
                                list.add(category);
                            }

                        }
                        if(list.size()!=0){
                            callback.onLoaded(list);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onCancelled(databaseError);
            }
        });
//        FirebaseInit.mDatabase.getReference("Movie").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                List<Category> categories = new ArrayList<>();
//                if (dataSnapshot != null) {
//                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                        if (postSnapshot != null) {
//                            Category category = postSnapshot.getValue(Category.class);
//                            categories.add(category);
//                        }
//                    }
//                    //Update UI here
//                    callback.onLoaded(categories);
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
////                showToast("Get all movie has stopped");
//                callback.onCancel(databaseError);
//            }
//        });
    }

    public void getSuggestionChannel(final DataSource.GetSuggCategoryCallback callback) {
        final List<Category> list = new ArrayList<>();
        mDatabase.getReference("User").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    list.clear();
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null && user.getCategory_id() != null) {
                        ArrayList<String> listCate = user.getCategory_id();
                        int size = listCate.size();
                        for (int i = 0; i < size; i++) {
                            if (listCate.get(i).equalsIgnoreCase("null")) {
                                Category category = new Category();
                                switch (i) {
                                    case 0:
                                        category.set_id("film-hai");
                                        category.setTitle("Phim hài");
                                        break;
                                    case 1:
                                        category.set_id("film-hanhdong");
                                        category.setTitle("Phim hành động");
                                        break;
                                    case 2:
                                        category.set_id("film-hoathinh");
                                        category.setTitle("Phim hoạt hình");
                                        break;
                                    case 3:
                                        category.set_id("film-kinhdi");
                                        category.setTitle("Phim kinh dị");
                                        break;
                                    case 4:
                                        category.set_id("film-mv");
                                        category.setTitle("Music Video");
                                        break;
                                    case 5:
                                        category.set_id("film-tinhcam");
                                        category.setTitle("Phim tình cảm");
                                        break;
                                    case 6:
                                        category.set_id("film-vientuong");
                                        category.setTitle("Phim viễn tưởng");
                                        break;
                                }
                                category.setThumnails(null);
                                category.setUser_id(null);
                                list.add(category);
                            }

                        }
                        if (list.size() != 0) {
                            callback.onLoaded(list);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onCancelled(databaseError);
            }
        });

    }
    public void subcribeChannel(final DataSource.SubscribeChannelCallback callback, String _id) {

//        FirebaseInit.mDatabase.getReference("User").child(FirebaseInit.mAuth.getCurrentUser().getUid()).child("category_id");
    }


    public void unsubcribeChannel(final DataSource.UnsubscribeChannelCallback callback, String _id) {
      //  _id = "-L-V3JvA_bHrcQ_rkbKs";
        String _id2 = "-L-V3JvA_bHrcQ_rkbKs9";

        char[] chars = _id2.toCharArray();
        final String final_id = _id;
        FirebaseInit.mDatabase.getReference("User").child("89as6d78a9sd7as89d7").child("category_id").child(chars[chars.length-1]+"").setValue("null").addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {
                FirebaseInit.mDatabase.getReference("Category").child(final_id).child("user_id").child("89as6d78a9sd7as89d7").setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
//                        showToast("Unsubcribe kênh thành công");
                        callback.onSuccess(aVoid);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
//                        showToast("Unsubcribe kênh thất bại");
                        callback.onFailture(e);
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                showToast("Unsubcribe kênh thất bại");
                callback.onFailture(e);

            }
        });
    }
    public void createProfile(final DataSource.CreateProfileUserCallback callback){
        User user = new User();
        user.set_id(FirebaseInit.mAuth.getCurrentUser().getUid());
        user.setAge(18);
        user.setAvatar("https://image.spreadshirtmedia.com/image-server/v1/mp/compositions/P102158672T347A386MPC105026736PA540PT14/views/1,width=100,height=100,appearanceId=386,backgroundColor=E8E8E8,version=1478262588/one-eyed-minion-women-s-t-shirt.jpg");
        user.setFullname("Mr "+new Date().getSeconds());
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i,"null");
        }
        user.setCategory_id(list);

        FirebaseInit.mDatabase.getReference("User").child(user.get_id()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onSuccess();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e);
            }
        });
    }



    public void checkSearch(final String keyword) {
        FirebaseInit.mDatabase.getReference("Movie").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Movie movie = postSnapshot.getValue(Movie.class);
                        if (removeAccent(movie.getTitle()).toLowerCase().trim().indexOf(keyword.toLowerCase()) > (-1)) {
                            Log.e("tag", movie.getTitle());
                            //result
//                            movie.getTitle();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "");
    }


    public void initNetwork() {

    }



    public void getSubcriptionChannel() {
        final String _uid = mAuth.getCurrentUser().getUid();
        final List<Category> list = new ArrayList<>();
        FirebaseInit.mDatabase.getReference("Category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Category category = postSnapshot.getValue(Category.class);
                    List<String> list_uid = category.getUser_id();
                    for (int i = 0; i < list_uid.size(); i++) {
                        if (list_uid.get(i) != null && !list_uid.get(i).equalsIgnoreCase(_uid)) {
                            Log.e("Subcription", category.get_id());
                            //Subcription
                            list.add(category);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getAllNotification(final DataSource.GetAllNotificationCallback callback){
        final List<Notification> list = new ArrayList<>();
        if(mAuth.getCurrentUser()!=null){
            FirebaseInit.mDatabase.getReference("Notification").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot!=null){
                        for (DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                            if(postSnapshot!=null){
                                Notification notification = postSnapshot.getValue(Notification.class);
                                list.add(notification);
                            }
                        }
                        callback.onLoaded(list);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    callback.onCancel(databaseError);
                }
            });
        }

    }

    public void pushNotification(Notification notification){
        String _key = FirebaseInit.mDatabase.getReference("Notification").push().getKey();
        FirebaseInit.mDatabase.getReference("Notification").child(_key).setValue(notification);
    }

    public void pushHistories(Movie movie) {
        String _uid = mAuth.getCurrentUser().getUid();
        History history = new History();
        history.set_id(FirebaseInit.mDatabase.getReference("History").push().getKey());
        history.setDate(new Date().getTime());
        history.setMovie_id(movie.get_id());
        history.setTitle(movie.getTitle());
        FirebaseInit.mDatabase.getReference("History").child(_uid).child(FirebaseInit.mDatabase.getReference("History").push().getKey()).setValue(history);
    }

    public void getRelatedMovie(String _id, final DataSource.GetVideoRelatedCallBack callBack) {
        final List<Movie> list = new ArrayList<>();
//        Log.d(FirebaseInit.class.getName(), _id);
        FirebaseInit.mDatabase.getReference("Movie").child(_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    final String category_id = (String) dataSnapshot.child("category_id").getValue();
                    final String title_movie = (String) dataSnapshot.child("title").getValue();

                    FirebaseInit.mDatabase.getReference("Movie").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot != null) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    Movie movie = postSnapshot.getValue(Movie.class);
                                    if(movie!=null){
//                                        if (movie.getCategoryName().equalsIgnoreCase(category_id) && !movie.getTitle().equalsIgnoreCase(title_movie)) {
//                                            list.add(movie);
//                                        }
                                        list.add(movie);
                                    }
                                }

                                callBack.onLoaded(list);
                                Log.d(FirebaseInit.class.getName(), list.toString());
                                //Update UI here
                                for (int i = 0; i < list.size(); i++) {
                                    Log.e("i"+i,list.get(i).getTitle());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
//                            showToast("Get related movie has stopped");
                            callBack.onCancel(databaseError);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                showToast("Get related movie has stopped");
            }
        });

    }

    public void getAllsChannel() {
        final List<Category> list = new ArrayList<>();
        FirebaseInit.mDatabase.getReference("Category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot != null) {
                            Category category = postSnapshot.getValue(Category.class);
                            list.add(category);
                        }
                    }
                    //Update UI here
                    for (int i = 0; i < list.size(); i++) {
                        Log.e("get alls channel: " + i, list.get(i).get_id() + "\n" + list.get(i).getTitle());
//                        showToast(list.get(i).get_id() + "\n" + list.get(i).getTitle());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                showToast("Get all channel has stopped");
            }
        });
    }
//    public void dialogNetwork() {
//        new AlertDialog.Builder(this).setMessage("Không có kết nối Internet !").setPositiveButton("Okie", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        }).setNegativeButton("Turn on Wifi", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//                wifi.setWifiEnabled(true);
//            }
//        }).show();
//    }
//
//    public boolean isGooglePlayService() {
//        boolean val = false;
//        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext()) == ConnectionResult.SUCCESS) {
//            val = true;
//        }
//        return val;
//    }

    public void newView(Movie movie) {
        FirebaseInit.mDatabase.getReference("Movie").child(movie.get_id()).child("view").setValue(movie.getView() + 1);
    }
    public void getTrendList(final DataSource.GetTrendCallback callback) {
        final List<Movie> list = new ArrayList<>();
        FirebaseInit.mDatabase.getReference("Movie").orderByChild("view").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot != null) {
                            Movie movie = postSnapshot.getValue(Movie.class);
                            list.add(movie);
                        }
                    }
                    callback.onLoaded(list);
                    Collections.reverse(list);
                    //Update UI here
                    int size = list.size();
//                    for (int i = 0; i < size; i++) {
//                        Log.e("get trend list", list.get(i).getTitle() + " - " + list.get(i).getView());
//                        showToast(list.get(i).getTitle() + " - " + list.get(i).getView());
//                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel(databaseError);
            }
        });
    }

    public void getAllsHistories(final DataSource.GetHistoriesCallback callback) {
        final List<History> list = new ArrayList<>();
        FirebaseInit.mDatabase.getReference("History").child(FirebaseInit.mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot != null) {
                            History history = postSnapshot.getValue(History.class);
                            list.add(history);
                        }
                    }
                    callback.onLoaded(list );
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel(databaseError);
            }
        });
    }
}