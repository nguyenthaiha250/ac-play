package com.android.c88.acplay.data.source.remote;

import android.util.Log;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.models.Category;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.Notification;

import java.util.List;

import javax.security.auth.callback.Callback;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public class RemoteDataSource implements DataSource {

    FirebaseInit mFirebaseInit;

    public RemoteDataSource(FirebaseInit firebaseInit) {
        this.mFirebaseInit = firebaseInit;
    }


    @Override
    public void getMovie(AddMovieCallback callback) {

    }

    @Override
    public void getMovies(GetMoviesCallback callback) {
        mFirebaseInit.getAllMovies(callback);
    }

    @Override
    public void getSubsCategory(GetSubsCategoryCallback callback) {
        mFirebaseInit.getSubcribedChannel(callback);
    }

    @Override
    public void getSuggCategory(GetSuggCategoryCallback callback) {
        mFirebaseInit.getSuggestionChannel(callback);
    }

    @Override
    public void getHistory(GetHistoryCallback callback) {

    }


    @Override
    public void getWatchLater(GetWatchLaterCallback callback) {

    }

    @Override
    public void getTrend(GetTrendCallback callback) {
        mFirebaseInit.getTrendList(callback);
    }

    @Override
    public void getDownload(GetDownloadCallback callback) {

    }

    @Override
    public void getVideoRelated(String id, GetVideoRelatedCallBack callBack) {
        mFirebaseInit.getRelatedMovie(id, callBack);
    }


    @Override
    public void getAllNotification(GetAllNotificationCallback callback) {
        mFirebaseInit.getAllNotification(callback);
    }

    @Override
    public void signInWithGuest(SignInWithGuestCallback callback) {
        Log.e("login","4");
            mFirebaseInit.signInWithGuest(callback);
    }

    @Override
    public void isGuest(IsGuestCallback callback) {
        mFirebaseInit.isGuest(callback);
    }

    @Override
    public void createProfileUser(CreateProfileUserCallback callback) {
        mFirebaseInit.createProfile(callback);
    }

    @Override
    public void getHistoriesOnline(GetHistoriesCallback callback) {
            mFirebaseInit.getAllsHistories(callback);
    }

    @Override
    public void pushHistories(Movie movie) {
        mFirebaseInit.pushHistories(movie);
    }

    @Override
    public void pushNotification(Notification notification) {
        mFirebaseInit.pushNotification(notification);
    }


    public void subscribeChannel(SubscribeChannelCallback callback, String s) {
        mFirebaseInit.subcribeChannel(callback, s);
    }

    @Override
    public void unsubscribeChannel(UnsubscribeChannelCallback callback, String s) {
        mFirebaseInit.unsubcribeChannel(callback, s);
    }

    //add movie to download data. Not use in RemoteDB
    @Override
    public void addMovieToDownload(Movie movie) {

    }

    //Not use in RemoteDB
    @Override
    public void addMovieToHistory(long time, Movie movie) {
    }

    //Not use in RemoteDB
    @Override
    public void addMovieToWatchLater(Movie movie) {

    }
}
