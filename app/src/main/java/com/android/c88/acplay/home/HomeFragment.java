package com.android.c88.acplay.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;


import com.android.c88.acplay.ACCustomFragment;
import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.R;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.databinding.FragmentHomeBinding;
import com.android.c88.acplay.media.VideoViewActivity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class HomeFragment extends ACCustomFragment {
    private static String TAG = "ACPlayHF";
    private FragmentHomeBinding mFragmentHomeBinding;
    private HomeViewModel mHomeViewModel;
    private VideoHomeAdapter adapter;
    private Intent intent;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeViewModel.start();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        mHomeViewModel = ACMainActivity.obtainHomeViewmodel(getActivity());
        mFragmentHomeBinding.setViewmodel(mHomeViewModel);
        adapter = new VideoHomeAdapter(getContext(), new ArrayList<Movie>(), mHomeViewModel);
        mFragmentHomeBinding.lvVideo.setAdapter(adapter);

        mFragmentHomeBinding.lvVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoViewActivity.KEY_MOVIE, mHomeViewModel.movies.get(i));
                intent.putExtras(bundle);
                new FirebaseInit().newView(mHomeViewModel.movies.get(i));
                startActivity(intent);

            }
        });
        return mFragmentHomeBinding.getRoot();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
