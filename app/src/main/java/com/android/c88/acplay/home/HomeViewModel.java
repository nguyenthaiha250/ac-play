package com.android.c88.acplay.home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;
import com.google.firebase.database.DatabaseError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by PhuocBH on 12/7/2017.
 */

public class HomeViewModel extends AndroidViewModel {

    private static String TAG = "ACPlayHVM";
    private Repository mRepository;
    public ObservableList<Movie> movies = new ObservableArrayList<>();
    public HomeViewModel(@NonNull Application application, Repository repository) {
        super(application);
        mRepository = repository;
    }

    public void start() {
        getMovies();
    }

    void getMovies() {
        movies.clear();
        mRepository.getMovies(new DataSource.GetMoviesCallback() {
            @Override
            public void onLoaded(List<Movie> movies) {
                for (Movie m : movies) {
                }
                Collections.reverse(movies);
                HomeViewModel.this.movies.addAll(movies);
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.e("getMovie", error.getMessage());
            }
        });
    }
}
