package com.android.c88.acplay.home;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Movie;

import java.util.List;

/**
 * Created by TuanTM13 on 12/7/2017.
 */

public class LvVideoBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:items")
    public static void setItems(ListView listView, List<Movie> items) {
        VideoHomeAdapter adapter = (VideoHomeAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.setMovies(items);
        }
    }
}
