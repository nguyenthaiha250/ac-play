package com.android.c88.acplay.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by HiepLH2 on 12/5/2017.
 */

public class VideoHomeAdapter extends BaseAdapter {
    Context context;

    List<Movie> movies;
    HomeViewModel mHomeViewModel;

    public VideoHomeAdapter(Context context, List<Movie> movies, HomeViewModel model) {
        this.context = context;
        setMovies(movies);
        mHomeViewModel = model;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_video, null);
            viewHolder = new ViewHolder();
            viewHolder.ivVideo = view.findViewById(R.id.imb_video);
            viewHolder.ivChannelAvatar = view.findViewById(R.id.imb_channel_avatar);
            viewHolder.txtTittle = view.findViewById(R.id.txt_tittle);
            viewHolder.txtDate = view.findViewById(R.id.txt_date);
            viewHolder.txtViewNum = view.findViewById(R.id.txt_viewNum);


            view.setTag(viewHolder);
        } else viewHolder = (ViewHolder) view.getTag();

        viewHolder.txtTittle.setText(movies.get(i).getTitle());

        Date date = new Date(movies.get(i).getDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        viewHolder.txtViewNum.setText(movies.get(i).getView() + "");

        String channel = "";
        switch (movies.get(i).getCategory_id()) {
            case "film-hai":
                channel = "Phim hài";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_funny));
                break;
            case "film-hanhdong":
                channel = "Phim hành động";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action));
                break;
            case "film-hoathinh":
                channel = "Phim hoạt hình";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_animation));
                break;
            case "film-kinhdi":
                channel = "Phim kinh dị";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_horror));
                break;
            case "film-mv":
                channel = "Music video";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_mv));
                break;
            case "film-tinhcam":
                channel = "Phim tình cảm";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_love));
                break;
            case "film-vientuong":
                channel = "Phim viễn tưởng";
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_vientuong));
                break;
        }
        viewHolder.txtDate.setText(channel + "  " + String.valueOf(simpleDateFormat.format(date)));

        Picasso.with(context)
                .load(movies.get(i).getThumnails())
                .error(R.drawable.loading_empty_img)
                .placeholder(R.drawable.loading_empty_img)
                .into(viewHolder.ivVideo);


        return view;
    }

    public class ViewHolder {
        ImageView ivVideo, ivChannelAvatar;
        TextView txtTittle, txtDate, txtViewNum;
    }
}
