package com.android.c88.acplay.library;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.c88.acplay.ACCustomFragment;
import com.android.c88.acplay.R;



public class LibraryFragment extends ACCustomFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_library, container, false);
        LibraryContainerFragment libraryContainerFragment=new LibraryContainerFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_library_container,libraryContainerFragment).addToBackStack(null).commit();
        return view;
    }
  /* private OnClickItemListener onClickItemListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_library, container, false);
        // Inflate the layout for this fragment
        RelativeLayout rlHistory = (RelativeLayout) view.findViewById(R.id.rl_history);
        RelativeLayout rlWatchLater = (RelativeLayout) view.findViewById(R.id.rl_watch_later);
        RelativeLayout rlDownload = (RelativeLayout) view.findViewById(R.id.rl_download);
        rlHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItemListener.onClicked(1);

            }
        });
        rlWatchLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItemListener.onClicked(2);
            }
        });
        rlDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItemListener.onClicked(3);
            }
        });
        if(!isOnline()){
            onClickItemListener.onClicked(4);
            rlHistory.setVisibility(View.GONE);
            rlWatchLater.setVisibility(View.GONE);

        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickItemListener) {
            onClickItemListener = (OnClickItemListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnClickItemListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onClickItemListener = null;
    }

    public interface OnClickItemListener {
        void onClicked(int position);
    }

    public void setOnClickItemListener(OnClickItemListener onClickItemListener) {
        this.onClickItemListener = onClickItemListener;
    }
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }*/
}
