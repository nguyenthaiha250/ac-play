package com.android.c88.acplay.library.download;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class DownloadAdapter  extends BaseAdapter {
    Context context;

    List<Movie> movies;
    DownloadViewModel mDownloadViewModel;

    public DownloadAdapter(Context context, List<Movie> movies, DownloadViewModel model) {
        this.context = context;
        setMovies(movies);
        mDownloadViewModel = model;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final DownloadAdapter.ViewHolder viewHolder;
        if (view==null){
            LayoutInflater inflater = LayoutInflater.from(context);
            view=inflater.inflate(R.layout.item_video,null);
            viewHolder = new DownloadAdapter.ViewHolder();
            viewHolder.ivVideo = view.findViewById(R.id.imb_video);
            viewHolder.ivChannelAvatar = view.findViewById(R.id.imb_channel_avatar);
            viewHolder.txtTittle=view.findViewById(R.id.txt_tittle);
            viewHolder.txtDate=view.findViewById(R.id.txt_date);
            viewHolder.txtViewNum=view.findViewById(R.id.txt_viewNum);

            view.setTag(viewHolder);
        }else viewHolder = (DownloadAdapter.ViewHolder) view.getTag();

        viewHolder.txtTittle.setText(movies.get(i).getTitle());
        viewHolder.txtDate.setText(movies.get(i).getDate()+"");
        viewHolder.txtViewNum.setText(movies.get(i).getView()+"");
        Picasso.with(context)
                .load(movies.get(i).getThumnails())
                .into(viewHolder.ivVideo);

    /*    final ConstraintLayout constraintLayout = view.findViewById(R.id.constraint_home);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewHolder.ivVideo.getLayoutParams();
      //  final float s  = constraintLayout.getWidth();
        params.width = 98;
        constraintLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                constraintLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int concho =constraintLayout.getHeight(); //height is ready
                Toast.makeText(context, concho+" 123", Toast.LENGTH_SHORT).show();
            }
        });*/

        //--------------------------------> Chưa xong đâu nha <------------------------
        Picasso.with(context)
                .load(movies.get(i).getSrc())
                .into(viewHolder.ivChannelAvatar);
        viewHolder.txtViewNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, movies.get(i).getView()+" lượt xem!! ", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
    public class ViewHolder {
        ImageView ivVideo,ivChannelAvatar;
        TextView txtTittle,txtDate,txtViewNum;
    }
}
