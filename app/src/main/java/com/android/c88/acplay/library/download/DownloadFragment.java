package com.android.c88.acplay.library.download;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.databinding.FragmentDownloadBinding;
import com.android.c88.acplay.databinding.FragmentHomeBinding;
import com.android.c88.acplay.home.HomeViewModel;
import com.android.c88.acplay.home.VideoHomeAdapter;
import com.android.c88.acplay.media.VideoViewActivity;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DownloadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DownloadFragment extends Fragment {
    private static String TAG = "DownloadFragment";
    private FragmentDownloadBinding mFragmentDownloadBinding;
    private DownloadViewModel mDownloadViewModel;
    private DownloadAdapter adapter;
    private Intent intent;

    public DownloadFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DownloadFragment newInstance(String param1, String param2) {
        return  new DownloadFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onResume() {
        super.onResume();
        mDownloadViewModel.start();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentDownloadBinding = FragmentDownloadBinding.inflate(inflater, container, false);
        mDownloadViewModel = ACMainActivity.obtainDownloadViewmodel(getActivity());
        mFragmentDownloadBinding.setViewmodel(mDownloadViewModel);
        adapter = new DownloadAdapter(getContext(), new ArrayList<Movie>(), mDownloadViewModel);
        mFragmentDownloadBinding.lvDownload.setAdapter(adapter);

        /*LinearLayout.LayoutParams lvVideoLParam = (LinearLayout.LayoutParams) mFragmentHomeBinding.lvVideo.getLayoutParams();
        lvVideoLParam.width = (int) 0.95f*ACMainActivity.SC_WIDTH;
        mFragmentHomeBinding.lvVideo.setLayoutParams(lvVideoLParam);*/

        mFragmentDownloadBinding.lvDownload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), mDownloadViewModel.movies.get(i).getTitle(), Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoViewActivity.KEY_MOVIE, (Serializable) mDownloadViewModel.movies);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return mFragmentDownloadBinding.getRoot();
        //return inflater.inflate(R.layout.fragment_download, container, false);
    }


}
