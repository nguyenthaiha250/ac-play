package com.android.c88.acplay.library.download;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.home.HomeViewModel;
import com.google.firebase.database.DatabaseError;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class DownloadViewModel extends AndroidViewModel {

    private static String TAG = "DownloadViewModel";
    private Repository mRepository;
    public ObservableList<Movie> movies = new ObservableArrayList<>();
    public DownloadViewModel(@NonNull Application application, Repository repository) {
        super(application);
        mRepository = repository;
    }

    public void start() {
        getMovies();
    }

    void getMovies() {
        movies.clear();
        mRepository.getDownload(new DataSource.GetDownloadCallback() {
            @Override
            public void onLoaded(List<Movie> movies) {
                DownloadViewModel.this.movies.addAll(movies);
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.e("getMovie", error.getMessage());
            }

        });
    }
}

