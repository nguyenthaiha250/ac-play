package com.android.c88.acplay.library.history;

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.TextView;

        import com.android.c88.acplay.R;
        import com.android.c88.acplay.data.models.History;

        import java.util.List;

/**
 * Created by THAIHA on 12/10/2017.
 */

public class HistoryAdapter extends BaseAdapter{
    Context context;

    List<History> histories;
    HistoryViewModel mHistoryViewModel;

    public HistoryAdapter(Context context, List<History> histories, HistoryViewModel model) {
        this.context = context;
        setHistories(histories);
        mHistoryViewModel = model;
    }

    public void setHistories(List<History> histories) {
        this.histories = histories;
     //   notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return histories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        HistoryAdapter.ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_history, null);
            viewHolder = new HistoryAdapter.ViewHolder();
            viewHolder.tvTittle = view.findViewById(R.id.tv_history_title);
            viewHolder.tvDate = view.findViewById(R.id.tv_history_date);
            view.setTag(viewHolder);
        } else viewHolder = (HistoryAdapter.ViewHolder) view.getTag();
        History history=histories.get(i);
        viewHolder.tvTittle.setText(history.getTitle());
        viewHolder.tvDate.setText(history.getDate() + "");
        return view;
    }

    public class ViewHolder {
       // ImageView ivVideo, ivChannelAvatar;
        TextView tvTittle, tvDate;
    }
}
