package com.android.c88.acplay.library.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.History;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.databinding.FragmentHistoryBinding;
import com.android.c88.acplay.databinding.FragmentHomeBinding;
import com.android.c88.acplay.home.HomeViewModel;
import com.android.c88.acplay.home.VideoHomeAdapter;
import com.android.c88.acplay.media.VideoViewActivity;

import java.io.Serializable;
import java.util.ArrayList;


public class HistoryFragment extends Fragment {

    private static String TAG = "HistoryFragment";
    private FragmentHistoryBinding mFragmentHistoryBinding;
    private HistoryViewModel mHistoryViewModel;
    private HistoryAdapter adapter;
    private Intent intent;

    public HistoryFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        return new HistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentHistoryBinding = FragmentHistoryBinding.inflate(inflater, container, false);
        mHistoryViewModel = ACMainActivity.obtainHistoryViewmodel(getActivity());
        mFragmentHistoryBinding.setViewmodel(mHistoryViewModel);
        adapter = new HistoryAdapter(getContext(), new ArrayList<History>(), mHistoryViewModel);
        mFragmentHistoryBinding.lvHistory.setAdapter(adapter);

       /* mFragmentHistoryBinding.lvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(getContext(), mHistoryViewModel.movies.get(i).getTitle(), Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("video",(Serializable) mHistoryViewModel.histories);
                intent.putExtra("BUNDLE",bundle);
                startActivity(intent);
            }
        });*/
        return mFragmentHistoryBinding.getRoot();
       // return inflater.inflate(R.layout.fragment_history, container, false);
    }
    @Override
    public void onResume() {
        super.onResume();
        mHistoryViewModel.start();
    }

}
