package com.android.c88.acplay.library.history;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.History;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.home.HomeViewModel;
import com.google.firebase.database.DatabaseError;

import java.util.HashMap;
import java.util.List;

/**
 * Created by THAIHA on 12/10/2017.
 */


public class HistoryViewModel  extends AndroidViewModel {

private static String TAG = "ACPlayHVM";
    private Repository mRepository;
    public ObservableList<History> histories = new ObservableArrayList<>();
    public HistoryViewModel(@NonNull Application application,Repository repository) {
        super(application);
        mRepository = repository;
    }
    public void start() {
        getHistories();
    }

    void getHistories() {
        histories.clear();
        if(FirebaseInit.mAuth.getCurrentUser()!=null){
            //Nếu đăng nhập nhannt25
            mRepository.getHistoriesOnline(new DataSource.GetHistoriesCallback() {
                @Override
                public void onLoaded(List<History> histories) {

                }

                @Override
                public void onCancel(DatabaseError error) {

                }
            });
        }else{
            //Nếu không đăng nhập nguyennv6
            mRepository.getHistory(new DataSource.GetHistoryCallback() {
                @Override
                public void onLoaded(HashMap<Long, Movie> histories) {

                }

                @Override
                public void onCancel(DatabaseError error) {

                }
            });
        }

    }


}
