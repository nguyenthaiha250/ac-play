
package com.android.c88.acplay.library.history;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.History;
import com.android.c88.acplay.data.models.Movie;


import java.util.List;
/*
*/
/**
 * Created by THAIHA on 12/10/2017.
 */


public class LvHistoryBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:itemsHis")
    public static void setHisItems(ListView listView, List<History> items) {
     HistoryAdapter adapter = (HistoryAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.setHistories(items);
        }
    }
}

