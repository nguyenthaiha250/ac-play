package com.android.c88.acplay.library.watchlater;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.home.VideoHomeAdapter;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class WatchLaterBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:itemsWatchLater")
    public static void setItems(ListView listView, List<Movie> items) {
        WatchLaterAdapter adapter = (WatchLaterAdapter) listView.getAdapter();
        if (adapter != null)
        {
            adapter.setMovies(items);
        }
    }
}
