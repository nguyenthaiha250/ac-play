package com.android.c88.acplay.library.watchlater;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.databinding.FragmentWatchLaterBinding;
import com.android.c88.acplay.media.VideoViewActivity;

import java.io.Serializable;
import java.util.ArrayList;


public class WatchLaterFragment extends Fragment {

    private static String TAG = "WatchLaterFragment";
    private FragmentWatchLaterBinding mFragmentWatchLaterBinding;
    private WatchLaterViewModel mWatchLaterViewModel;
    private WatchLaterAdapter adapter;
    private Intent intent;


    public WatchLaterFragment() {
        // Required empty public constructor
    }


    public static WatchLaterFragment newInstance(String param1, String param2) {
        return new WatchLaterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mWatchLaterViewModel.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mFragmentWatchLaterBinding = FragmentWatchLaterBinding.inflate(inflater, container, false);
        mWatchLaterViewModel = ACMainActivity.obtainWatchLaterViewmodel(getActivity());
        mFragmentWatchLaterBinding.setViewmodel(mWatchLaterViewModel);
        adapter = new WatchLaterAdapter(getContext(), new ArrayList<Movie>(), mWatchLaterViewModel);
        mFragmentWatchLaterBinding.lvWatchLater.setAdapter(adapter);

        /*LinearLayout.LayoutParams lvVideoLParam = (LinearLayout.LayoutParams) mFragmentHomeBinding.lvVideo.getLayoutParams();
        lvVideoLParam.width = (int) 0.95f*ACMainActivity.SC_WIDTH;
        mFragmentHomeBinding.lvVideo.setLayoutParams(lvVideoLParam);*/

        mFragmentWatchLaterBinding.lvWatchLater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), mWatchLaterViewModel.movies.get(i).getTitle(), Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoViewActivity.KEY_MOVIE, (Serializable) mWatchLaterViewModel.movies);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return mFragmentWatchLaterBinding.getRoot();
       // return inflater.inflate(R.layout.fragment_watch_later, container, false);
    }

}
