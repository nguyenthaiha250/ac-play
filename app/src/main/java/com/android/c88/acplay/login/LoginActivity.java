package com.android.c88.acplay.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.R;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.data.source.remote.RemoteDataSource;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity {

    SignInButton signInButton;
    TextView button;
    ImageView imageView;
    private FirebaseAuth mAuth;
    private LoginViewModel loginViewModel;
    private Repository mRepository;
    private RemoteDataSource mRemote;

    private GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Window window = LoginActivity.this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.acColor));
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mAuth = FirebaseAuth.getInstance();
        FirebaseInit firebaseInit = new FirebaseInit();
        mRemote = new RemoteDataSource(firebaseInit);
        mRepository = new Repository(mRemote, null);
        loginViewModel = new LoginViewModel(mRepository, LoginActivity.this);
        if (mAuth.getCurrentUser() != null) {
            button.setVisibility(View.GONE);
            signInButton.setVisibility(View.GONE);
        }

        //Check authentication
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mAuth.getCurrentUser() != null) {
                    startActivity(new Intent(LoginActivity.this, ACMainActivity.class));
                    Toast.makeText(getApplicationContext(), "Hi " + FirebaseInit.mAuth.getCurrentUser().getUid() + " !", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }, 1000);


        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(LoginActivity.this,ACMainActivity.class));
//                Toast.makeText(LoginActivity.this, "Google authen not available", Toast.LENGTH_SHORT).show();
//                finish();
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 123);
            }
        });
        imageView = findViewById(R.id.imageView5);
        Animation animation = android.view.animation.AnimationUtils.loadAnimation(LoginActivity.this, R.anim.slide_up);
        imageView.setAnimation(animation);

        TextView textView = (TextView) signInButton.getChildAt(0);
        textView.setText("Sign in with Google+");
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Login with Guest", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, ACMainActivity.class));
                finish();
            }
        });


        if (mAuth.getCurrentUser() != null) {
            button.setClickable(false);
            button.setEnabled(false);
        }
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button.setTextColor(getResources().getColor(R.color.colorBlack));
                        break;
                    case MotionEvent.ACTION_UP:
                        button.setTextColor(getResources().getColor(R.color.colorDarkGray));
                        break;
                    default:
                }
                return false;
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 123) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.e("TAG", String.valueOf(e));
                Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.e("TAG", "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            if (mAuth.getCurrentUser() != null) {
                                startActivity(new Intent(LoginActivity.this, ACMainActivity.class));
                                finish();

                            }
                            Log.e("TAG", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            Toast.makeText(LoginActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

}
