package com.android.c88.acplay.login;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.source.remote.FirebaseInit;

/**
 * Created by NhanNT25 on 12/11/2017.
 */

public class LoginViewModel extends AndroidViewModel {

    private static String TAG = "ACPlayHVM";
    private Repository mRepository;
    private Context context;


    public LoginViewModel(Repository mRepository, Context context) {
        super(null);
        this.mRepository = mRepository;
        this.context = context;
    }

    void signInWithGuest(){
        Log.e("login","2");
        mRepository.signInWithGuest(new DataSource.SignInWithGuestCallback() {
            @Override
            public void onSuccess() {
                createProfileDefault();
                Toast.makeText(context, "Hi Guest!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent (context,ACMainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(i);
            }

            @Override
            public void onFailure(Exception error) {
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_SHORT).show();
                Log.e("TAG", "error");

            }
        });
    }
    void isGuest(){
        mRepository.isGuest(new DataSource.IsGuestCallback() {
            @Override
            public void isTrue(boolean isTrue) {
                if(isTrue){
                    Toast.makeText(context,"Hi Guest ID:"+ FirebaseInit.mAuth.getCurrentUser().getUid(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "You not guest", Toast.LENGTH_SHORT).show();
                    if(FirebaseInit.mAuth.getCurrentUser()!=null){
                        Toast.makeText(context, FirebaseInit.mAuth.getCurrentUser().getUid()+":User", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    void createProfileDefault(){
        mRepository.createProfileUser(new DataSource.CreateProfileUserCallback() {
            @Override
            public void onSuccess() {
                Toast.makeText(context, "Create success profile", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(context, String.valueOf(e), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
