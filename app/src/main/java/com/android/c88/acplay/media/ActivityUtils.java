package com.android.c88.acplay.media;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Hung on 12/11/2017.
 */

public class ActivityUtils {
    public static void replaceFragmentInActivity(@NonNull FragmentManager fragMan, @NonNull Fragment frag, int frameId){
        FragmentTransaction transaction = fragMan.beginTransaction();
        transaction.replace(frameId, frag);
        transaction.commit();
    }
}
