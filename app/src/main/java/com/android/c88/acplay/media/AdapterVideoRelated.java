//package com.android.c88.acplay.media;
//
//import android.databinding.DataBindingUtil;
//import android.databinding.ViewDataBinding;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.android.c88.acplay.R;
//import com.android.c88.acplay.data.models.Movie;
//import com.android.c88.acplay.data.models.User;
//import com.android.c88.acplay.databinding.ItemVideoSameBinding;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Hung on 12/11/2017.
// */
//
//public class AdapterVideoRelated extends RecyclerView.Adapter<AdapterVideoRelated.BindingViewHolder>{
//
//    private VideoRelatedViewModel viewModel;
//    List<Movie> listMovie;
//
//    OnItemVideoSameClickListener onItemVideoClick;
//
//    public AdapterVideoRelated(VideoRelatedViewModel viewModel, List<Movie> listMovie) {
//        this.viewModel = viewModel;
//        setListMovie(listMovie);
//    }
//
//    public AdapterVideoRelated(OnItemVideoSameClickListener onItemVideoClick) {
//        this.onItemVideoClick = onItemVideoClick;
//    }
//
//    @Override
//    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//
//        ItemVideoSameBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);
//
//        return new BindingViewHolder(binding);
//    }
//
//    @Override
//    public void onBindViewHolder(BindingViewHolder holder, int position) {
//
//        holder.itemBinding.setMovie(listMovie.get(position));
//
//        holder.itemBinding.executePendingBindings();
//
//        Log.d("ADAPTER VIDEO RELATED", listMovie.get(position).getCategoryName());
//    }
//
//    @Override
//    public int getItemCount() {
//        return listMovie == null ? 0 : listMovie.size();
//    }
//
//    public class BindingViewHolder extends RecyclerView.ViewHolder{
//
//        private final ItemVideoSameBinding itemBinding;
//
//        public BindingViewHolder(ItemVideoSameBinding itemBinding) {
//            super(itemBinding.getRoot());
//            this.itemBinding = itemBinding;
//        }
//
//
//    }
//
//    public void replaceData(List<Movie> movies){
//        setListMovie(movies);
//    }
//
//    private void setListMovie(List<Movie> movies){
//        listMovie = movies;
//        notifyDataSetChanged();
//
//        Log.d("LIST ADAPETR", listMovie.size() + "");
//    }
//}
