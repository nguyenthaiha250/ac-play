package com.android.c88.acplay.media;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HungVP1 on 12/6/2017.
 */

public class AdapterVideoSame extends RecyclerView.Adapter<AdapterVideoSame.MyViewHolder> {

    private List<Movie> listVideoSame = new ArrayList<>();
    private int rowLayout;
    private Context myContext;
    private VideoRelatedViewModel viewModel;
    private OnItemVideoSameClickListener onItemVideoClick;

    public AdapterVideoSame(List<Movie> listVideoSame, int rowLayout, Context myContext, VideoRelatedViewModel viewModel) {
        setListMovie(listVideoSame);
        this.rowLayout = rowLayout;
        this.myContext = myContext;
        this.viewModel = viewModel;
    }

    public void setListMovie(List<Movie> movies){
        listVideoSame = movies;
        notifyDataSetChanged();

        Log.d("LIST ADAPETR", listVideoSame.size() + "");
    }

    public void setOnItemVideoClick(OnItemVideoSameClickListener onItemVideoClick) {
        this.onItemVideoClick = onItemVideoClick;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(itemView);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Movie getItem = listVideoSame.get(position);

        holder.txtItemTitle.setText(getItem.getTitle());
        holder.txtItemNameChanel.setText(getItem.getCategoryName());
        holder.txtItemNumberView.setText(getItem.getView() + " views");
        holder.txtItemDuration.setText(getItem.getDuration() + "");
        Log.d("SAME_ADAPTER", getItem.getCategoryName() );
        Picasso.with(myContext)
                .load(getItem.getThumnails())
                .error(R.drawable.loading_empty_img)
                .placeholder(R.drawable.loading_empty_img)
                .into(holder.imgThumbnail);
    }

    @Override
    public int getItemCount() {
        return listVideoSame == null ? 0 : listVideoSame.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            PopupMenu.OnMenuItemClickListener{

        TextView txtItemTitle, txtItemNameChanel, txtItemNumberView, txtItemDuration, btnOptionMenuItem;
        LinearLayout linearLayoutItem;
        ImageView imgThumbnail;

        public MyViewHolder(final View itemView) {
            super(itemView);

            txtItemTitle = itemView.findViewById(R.id.item_title);
            txtItemNameChanel = itemView.findViewById(R.id.name_chanel_item);
            txtItemNumberView = itemView.findViewById(R.id.item_number_view);
            txtItemDuration = itemView.findViewById(R.id.duration_item);
            btnOptionMenuItem = itemView.findViewById(R.id.btn_option_menu_item);
            linearLayoutItem = itemView.findViewById(R.id.layout_item_rcv);
            imgThumbnail = itemView.findViewById(R.id.img_video_same);

//            itemView.setTag(this);
            btnOptionMenuItem.setTag(this);

            linearLayoutItem.setOnClickListener(this);
            btnOptionMenuItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R.id.layout_item_rcv:
                    onItemVideoClick.onItemVideoClick(v, getAdapterPosition());
                    break;

                case R.id.btn_option_menu_item:
                    // get my view holder
                    MyViewHolder myViewHolder = (MyViewHolder) v.getTag();
                    PopupMenu popupOptionMenu = new PopupMenu(myContext, myViewHolder.btnOptionMenuItem);
                    popupOptionMenu.inflate(R.menu.option_menu_item);
                    popupOptionMenu.setOnMenuItemClickListener(this);
                    popupOptionMenu.show();
                    break;
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.item_option_watch_later:
                    Toast.makeText(myContext, "Add" + listVideoSame.get(getAdapterPosition()).getTitle()
                            + " " + getAdapterPosition() + " to Watch later", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.item_option_download:

                    break;
            }
            return false;
        }
    }

        public void replaceData(List<Movie> movies){
        setListMovie(movies);
    }



}
