package com.android.c88.acplay.media;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;

/**
 * Created by NguyenNV6 on 12/11/2017.
 */

public class DownloadTask extends AsyncTask {
    private Movie movie;
    private Context context;
    private String urlVideo, fileNameVideo, urlThumb, fileNameThumb, thumbSrc, videoSrc;
    private DownloadManager downloadManager;
    private DownloadManager.Request request, request1;
    private long downloadReference, downloadReference1;
    private BroadcastReceiver onComplete;
    Repository mRepository;

    public DownloadTask(Context context, Movie movie) {
        this.context = context;
        this.movie = movie;
        fileNameVideo = movie.get_id() + ".mp4";
        urlVideo = movie.getSrc();
        fileNameThumb = movie.get_id() + ".jpg";
        urlThumb = movie.getThumnails();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(context, "Start downloading.......", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//        DownloadManager downloadManager1 = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        //Download Thumbnail
        request1 = new DownloadManager.Request(Uri.parse(urlThumb));
        request1.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request1.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request1.setTitle(fileNameThumb);
        request1.setDescription("Download using Download Manager");
        request1.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_PICTURES, fileNameThumb);
        //get url thumbnail video in External Storage
        thumbSrc = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath() + "/" + fileNameThumb;
        Log.d("Link src:", thumbSrc);

// Download video
        request = new DownloadManager.Request(Uri.parse(urlVideo));
        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setTitle(fileNameVideo);
        request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_MOVIES, fileNameVideo);
        //Enqueue download and save into referenceID
        downloadReference = downloadManager.enqueue(request);
        downloadReference1 = downloadManager.enqueue(request1);
        // get url video in External Storage
        videoSrc = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES).getPath() + "/" + fileNameVideo;
        Log.d("Link src:", videoSrc);

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long referenceID = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (referenceID == downloadReference1) {
                    Toast.makeText(context, "Download Image Done! ahihihihi", Toast.LENGTH_SHORT).show();
                } else if (referenceID == downloadReference) {
                    Toast.makeText(context, "Download Video Done! :))))", Toast.LENGTH_SHORT).show();
                }
                Log.d("DONEEEEEEEEEEEEEEEEEE:", "Download Complete!");

            }
        };
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        movie.setThumnails(thumbSrc);
        movie.setSrc(videoSrc);
        mRepository.addMovieToDownload(movie);
    }
}
