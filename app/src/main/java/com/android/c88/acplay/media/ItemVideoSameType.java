package com.android.c88.acplay.media;

/**
 * Created by HungVP1 on 12/6/2017.
 */

public class ItemVideoSameType {
    String title, nameChanel, duration;
    Double numberView;

    public ItemVideoSameType(String title, String nameChanel, Double numberView, String duration) {
        this.title = title;
        this.nameChanel = nameChanel;
        this.numberView = numberView;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNameChanel() {
        return nameChanel;
    }

    public void setNameChanel(String nameChanel) {
        this.nameChanel = nameChanel;
    }

    public Double getNumberView() {
        return numberView;
    }

    public void setNumberView(Double numberView) {
        this.numberView = numberView;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
