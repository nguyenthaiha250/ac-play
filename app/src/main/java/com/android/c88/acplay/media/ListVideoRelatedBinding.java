package com.android.c88.acplay.media;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.android.c88.acplay.data.models.Movie;

import java.util.List;

/**
 * Created by Hung on 12/11/2017.
 */

public class ListVideoRelatedBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:listVideo")
    public static void setListVideo(RecyclerView rcv, List<Movie> movies){
        AdapterVideoSame adapterVideoRelated = (AdapterVideoSame) rcv.getAdapter();

        if (adapterVideoRelated != null){
            adapterVideoRelated.setListMovie(movies);
        }
    }
}
