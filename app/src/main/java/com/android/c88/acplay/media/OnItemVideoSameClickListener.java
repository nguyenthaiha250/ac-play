package com.android.c88.acplay.media;

import android.view.View;

/**
 * Created by HungVP1 on 12/6/2017.
 */

public interface OnItemVideoSameClickListener {
    void onItemVideoClick(View view, int position);
}
