package com.android.c88.acplay.media;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.User;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 12/10/2017.
 */

public class  VideoRelatedViewModel extends AndroidViewModel{

    public final ObservableList<Movie> listVideoRelated = new ObservableArrayList<>();

    public final ObservableField<Movie> fieldMovies = new ObservableField<>();

    Repository mRepo;

    public VideoRelatedViewModel(@NonNull Application application, Repository mRepo) {
        super(application);
        this.mRepo = mRepo;
    }

    public void start(String id){
        getListVideoRelated(id);

    }

    public void getListVideoRelated(String id){

        mRepo.getVideoRelated(id, new DataSource.GetVideoRelatedCallBack() {
            @Override
            public void onLoaded(List<Movie> movies) {
                Log.d("XEM LIST", movies.toString());

                for (Movie movie : movies){

                }

                VideoRelatedViewModel.this.listVideoRelated.addAll(movies);

            }

            @Override
            public void onCancel(DatabaseError error) {

            }
        });

    }
}
