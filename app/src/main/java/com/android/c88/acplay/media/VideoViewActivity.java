package com.android.c88.acplay.media;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.c88.acplay.R;
import com.android.c88.acplay.ViewModelFactory;
import com.android.c88.acplay.data.models.Movie;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

public class VideoViewActivity extends AppCompatActivity implements ReceiveMovieFromHomeAct{

    public static final String KEY_MOVIE = "MOVIE";

//    ObservableList<Movie> arrMovie = new ObservableArrayList<>();

    VideoRelatedViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        VideoViewFragment fragment = setupViewFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), fragment, R.id.frame_content);
        viewModel = obtainViewModel(this);

    }

    public static VideoRelatedViewModel obtainViewModel(FragmentActivity activity){
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        VideoRelatedViewModel viewModel = ViewModelProviders.of(activity, factory).get(VideoRelatedViewModel.class);

        return viewModel;
    }

    private VideoViewFragment setupViewFragment(){
        VideoViewFragment fragment = (VideoViewFragment) getSupportFragmentManager().findFragmentById(R.id.frame_content);

        if (fragment == null){
            Movie movie = (Movie) getIntent().getSerializableExtra(VideoViewActivity.KEY_MOVIE);
            fragment = VideoViewFragment.newInstance(movie);

//            Log.d(VideoViewActivity.class.getName(), movie.getCategoryName());
        }

        return fragment;
    }

    @Override
    public void onReceiveMovie() {
//        Intent intent = getIntent();
//        Bundle bundle = intent.getExtras();
//        arrMovie = (ObservableList<Movie>) bundle.getSerializable(KEY_MOVIE);
    }

}
