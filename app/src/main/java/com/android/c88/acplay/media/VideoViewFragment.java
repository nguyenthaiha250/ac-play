package com.android.c88.acplay.media;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.databinding.ObservableField;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;

import com.android.c88.acplay.databinding.FragmentVideoViewBinding;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Hung on 12/11/2017.
 */

public class VideoViewFragment extends Fragment implements BetterVideoCallback {

    public static final String KEY_PASS_MOVIE_TO_FRAG = "PASS_MOVIE";

    private VideoRelatedViewModel viewModel;
    private FragmentVideoViewBinding fragmentBinding;
    private AdapterVideoSame adapter;
    private List<Movie> arrMovie = new ArrayList<>();
    private Movie movie;

//    ---------------------------VideoPlayer--------------------------------------
    private static final String TAG = "VIDEOPLAYERLOG";
    private BetterVideoPlayer player;
    private Uri uri;
    private Window window;
    private ImageView fullScreenImage;
    private ViewGroup.LayoutParams lp;
    private SharedPreferences sharedPreferences;
    private FrameLayout fullScreenButton;

//    ------------------------------------------------------------------------------

    public VideoViewFragment() {

    }

    public static VideoViewFragment newInstance(Movie movie){
        Bundle arguments = new Bundle();
        arguments.putSerializable(KEY_PASS_MOVIE_TO_FRAG, movie);
        VideoViewFragment fragment = new VideoViewFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentBinding = FragmentVideoViewBinding.inflate(inflater, container, false);

        viewModel = VideoViewActivity.obtainViewModel(getActivity());

        fragmentBinding.setVideoViewModel(viewModel);

        RecyclerView rcv = fragmentBinding.listVideoRelated;

        adapter = new AdapterVideoSame(arrMovie, R.layout.item_video_same, getActivity().getApplicationContext(), viewModel);
        Log.d(TAG, "list movie have " + arrMovie.size() + "");
        rcv.setAdapter(adapter);

        return fragmentBinding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupListVideoAdapter();
        //        ---------------------------->MEDIA_PLAYER<--------------------

        sharedPreferences = getActivity().getSharedPreferences("PositionPref",MODE_PRIVATE);
        // Grabs a reference to the player view
        initMediaPlayerView();
        initMediaPlayerFullScreen();
        initMediaPlayer();
//        ----------------------------------------------------------------------

//        showInfoVideo();
    }

    @Override
    public void onResume() {
        super.onResume();
        movie = (Movie) getArguments().getSerializable(KEY_PASS_MOVIE_TO_FRAG);
//        showInfoVideo();

        if(getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
            ViewGroup.LayoutParams lpF = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            player.setLayoutParams(lpF);
            fullScreenImage.setImageResource(R.drawable.ic_fullscreen_skrink);
            player.enableSwipeGestures(window);
        }
        else {
            fullScreenImage.setImageResource(R.drawable.ic_fullscreen_expand);
            player.setLayoutParams(lp);
        }
        uri = Uri.parse(movie.getSrc());
//        uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/Movies/cham_khe_tim_anh_mot_chut_thoi.mp4"));
        player.setSource(uri);
        player.getToolbar().setTitle(movie.getTitle());
        player.setAutoPlay(true);
        Log.d(TAG,movie.get_id() + movie.getTitle() + uri.toString());
    }

    private void setupListVideoAdapter(){

//        viewModel.getListVideoRelated(movie.getCategory_id());


    }

    private void showInfoVideo(){
        if (getArguments() != null){
            viewModel.start(movie.get_id());
        } else {
            viewModel.start(null);
        }
    }


    //        ---------------------------->ACTIVITY_OVERRIDE<--------------------

    @Override
    public void onPause() {
        super.onPause();
        // Make sure the player stops playing if the user presses the home button.
        player.pause();
    }

    //    --------------------------------------FUNCTION--------------------------------------------

    public void initMediaPlayerView(){
        player = getActivity().findViewById(R.id.player);
        fullScreenButton = player.findViewById(R.id.fullscreen_button);
        fullScreenImage = player.findViewById(R.id.fullscreen_icon);
    }

    public void initMediaPlayerFullScreen(){
        lp =  player.getLayoutParams();
        fullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putInt(movie.get_id(),player.getCurrentPosition()).apply();
                if(getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                else if(getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE){
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
    }

    public void initMediaPlayer(){
        ContentResolver cr = getActivity().getContentResolver();
        window = getActivity().getWindow();
        Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        try {
            int brightness = Settings.System.getInt(cr,Settings.System.SCREEN_BRIGHTNESS)/255*100;
            window.getAttributes().screenBrightness = brightness;
            Log.d(TAG,brightness+"");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        // Sets the callback to this Activity, since it inherits EasyVideoCallback
        player.setCallback(this);
        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        //Test trong may
        //Test online

        // From here, the player view will show a progress indicator until the player is prepared.
        // Once it's prepared, the progress indicator goes away and the controls become enabled for the user to begin playback.

    }

    public int getScreenOrientation()
    {
        Display getOrient = getActivity().getWindowManager().getDefaultDisplay();
        int orientation;
        if(getOrient.getWidth()==getOrient.getHeight()){
            orientation = Configuration.ORIENTATION_SQUARE;
        } else{
            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

//    -----------------------------------------------------------------------------------------

    //        ---------------------------->MEDIA_PLAYER_OVERRIDE<--------------------
    @Override
    public void onStarted(BetterVideoPlayer player) {
        Log.i(TAG, "Started");
        player.seekTo(sharedPreferences.getInt(movie.get_id(),0));
    }

    @Override
    public void onPaused(BetterVideoPlayer player) {
        sharedPreferences.edit().putInt(movie.get_id(),player.getCurrentPosition()).apply();
        Log.i(TAG, "Paused");
    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {
        Log.i(TAG, "Preparing");
    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {
        Log.i(TAG, "Prepared");

    }

    @Override
    public void onBuffering(int percent) {
        Log.i(TAG, "Buffering " + percent);
    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {
        player.getToolbar().setSubtitle("video is not available, please try again");
        Log.i(TAG, "Error " +e.getMessage());
    }

    @SuppressLint("ResourceType")
    @Override
    public void onCompletion(BetterVideoPlayer player) {
        sharedPreferences.edit().putInt(movie.get_id(),0).apply();
        Log.i(TAG, "Completed");
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {
        Log.i(TAG, "Controls toggled " + isShowing);
    }

//    ----------------------------------------------------------------------------------------


}
