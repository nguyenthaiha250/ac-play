package com.android.c88.acplay.search;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.media.AdapterVideoSame;
import com.android.c88.acplay.media.VideoViewActivity;
import com.android.c88.acplay.media.VideoViewFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultSearchFragment extends Fragment {
    private ListView lv;
    private AdapterVideoSame adapterVideoSame;
    private FirebaseInit firebaseInit;
    ArrayAdapter arrayAdapter;
    String src;
    private Movie movie;
    // private ArrayList<ItemVideoSameType> list;
    public ResultSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result_search, container, false);
        lv = (ListView) view.findViewById(R.id.lv_result);

        firebaseInit = new FirebaseInit();
        String checkword  = this.getArguments().getString("key");
        checkSearch(checkword);
        //Toast.makeText(getActivity(), checkword, Toast.LENGTH_SHORT).show();


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "");
    }

    public void checkSearch(final String keyword) {
        FirebaseInit.mDatabase.getReference("Movie").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                    final List<String> list = new ArrayList<>();
                    final List<Movie> movieList = new ArrayList<>();
                    if (dataSnapshot != null) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            final Movie movie = postSnapshot.getValue(Movie.class);
                            if (removeAccent(movie.getTitle()).toLowerCase().trim().indexOf(removeAccent(keyword.toLowerCase())) > (-1)) {

                                movieList.add(movie);
                                Log.d("TAG", movie.getTitle());
                                //list.add(new ItemVideoSameType(title,category,Double.valueOf(view),duration));
                                list.add(movie.getTitle());
                                arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, list);
                                lv.setAdapter(arrayAdapter);
                                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent i = new Intent(getActivity(), VideoViewActivity.class);
                                        Bundle b = new Bundle();
                                        b.putSerializable(VideoViewActivity.KEY_MOVIE, ( movieList.get(position)));
                                        i.putExtras(b);
                                        Log.d("itemClick", movieList.get(position).getTitle());
                                        startActivity(i);
                                    }
                                });

                            }

//                ArrayList<ItemVideoSameType> list = new ArrayList<>();
//                AdapterVideoSame arrayAdapter = new AdapterVideoSame(list,R.layout.item_video_same,ResultSearchActivity.this);

                        }

                    }
                }


                catch (Exception e){
                    Log.d("TAG", e.toString());
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
