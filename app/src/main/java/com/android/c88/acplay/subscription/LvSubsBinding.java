package com.android.c88.acplay.subscription;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Category;

import java.util.List;

/**
 * Created by HiepLH2 on 12/8/2017.
 */

public class LvSubsBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:itemsSubs")
    public static void setItemSubs(ListView listView,List<Category> categories){
        SubscribedAdapter adapter = (SubscribedAdapter) listView.getAdapter();
        if (adapter!=null){
            adapter.setCategories(categories);
        }
    }
}
