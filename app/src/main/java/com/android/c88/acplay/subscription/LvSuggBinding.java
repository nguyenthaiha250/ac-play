package com.android.c88.acplay.subscription;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Category;

import java.util.List;

/**
 * Created by HiepLH2 on 12/9/2017.
 */

public class LvSuggBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:itemsSugg")
    public static void setItemSugg(ListView listView, List<Category> categories){
        SuggestionAdapter adapter = (SuggestionAdapter) listView.getAdapter();
        if(adapter!=null)
            adapter.setCategories(categories);
    }
}
