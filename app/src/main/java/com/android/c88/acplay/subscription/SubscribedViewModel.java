package com.android.c88.acplay.subscription;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Category;
import com.google.firebase.database.DatabaseError;

import java.util.List;

/**
 * Created by HiepLH2 on 12/8/2017.
 */

public class SubscribedViewModel extends AndroidViewModel {
    private static String TAG = "ACPlaySubVM";
    private Repository mRepository;
    public ObservableList<Category> categories = new ObservableArrayList<>();

    public SubscribedViewModel(@NonNull Application application, Repository mRepository) {
        super(application);
        this.mRepository = mRepository;
    }
    public void start() {
        getCategories();
    }

    void getCategories() {
        mRepository.getSubsCategory(new DataSource.GetSubsCategoryCallback() {
            @Override
            public void onLoaded(List<Category> categories) {
                SubscribedViewModel.this.categories.clear();
                SubscribedViewModel.this.categories.addAll(categories);
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.e("getMovie", error.getMessage());
            }
        });
    }

    void unsubscribeChannel(String s){
        mRepository.unsubscribeChannel(new DataSource.UnsubscribeChannelCallback() {
            @Override
            public void onSuccess(Void aVoid) {
                getCategories();
            }

            @Override
            public void onFailture(Exception e) {

            }
        }, s);
    }
}
