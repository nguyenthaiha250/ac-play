package com.android.c88.acplay.subscription;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.c88.acplay.R;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.data.models.User;
import com.android.c88.acplay.data.source.remote.FirebaseInit;
import com.android.c88.acplay.data.models.Category;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HiepLH2 on 12/5/2017.
 */

public class SuggestionAdapter extends BaseAdapter {
    SuggestionViewModel suggestionViewModel;
    List<Category> categories = new ArrayList<>();
    Context context;

    public SuggestionAdapter(SuggestionViewModel suggestionViewModel, List<Category> categories, Context context) {
        this.suggestionViewModel = suggestionViewModel;
        this.categories = categories;
        this.context = context;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.item_suggestion, null);
            viewHolder = new ViewHolder();
            viewHolder.ivChannelAvatar = view.findViewById(R.id.iv_avatar_suggestion);
            viewHolder.txtTitle = view.findViewById(R.id.txt_title_suggestion);
            viewHolder.txtId=view.findViewById(R.id.txt_sugg_id);
            view.setTag(viewHolder);
        } else viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtId.setText(categories.get(i).get_id());
        switch (categories.get(i).get_id()){
            case "film-hai":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_funny));
                break;
            case "film-hanhdong":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action));
                break;
            case "film-hoathinh":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_animation));
                break;
            case "film-kinhdi":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_horror));
                break;
            case "film-mv":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_mv));
                break;
            case "film-tinhcam":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_love));
                break;
            case "film-vientuong":
                viewHolder.ivChannelAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_vientuong));
                break;
        }
        Log.e("suggestion adapter",categories.get(i).get_id());
        viewHolder.txtTitle.setText(categories.get(i).getTitle()+" (0)");
        FirebaseInit.mDatabase.getReference("Movie").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int count = 0;
                if(dataSnapshot!=null){
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                        Movie movie = postSnapshot.getValue(Movie.class);
                        if(movie!=null){
                            if(categories.get(i).get_id().equalsIgnoreCase(movie.getCategory_id())){
                                count++;
                            }
                        }
                    }
                    if(count>0){
                        viewHolder.txtTitle.setText(categories.get(i).getTitle()+" ("+count+"+)");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseInit.mDatabase.getReference("User").child(FirebaseInit.mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot!=null){
                            User user =dataSnapshot.getValue(User.class);
                            ArrayList<String> list = user.getCategory_id();
                            if(user!=null){
                                switch (categories.get(i).get_id()){
                                    case "film-hai":
                                        list.set(0,"film-hai");
                                        break;
                                    case "film-hanhdong":
                                        list.set(1,"film-hanhdong");
                                        break;
                                    case "film-hoathinh":
                                        list.set(2,"film-hoathinh");
                                        break;
                                    case "film-kinhdi":
                                        list.set(3,"film-kinhdi");
                                        break;
                                    case "film-mv":
                                        list.set(4,"film-mv");
                                        break;
                                    case "film-tinhcam":
                                        list.set(5,"film-tinhcam");
                                        break;
                                    case "film-vientuong":
                                        list.set(6,"film-vientuong");
                                        break;

                                }
                            }
                            user.setCategory_id(list);
                            FirebaseInit.mDatabase.getReference("User").child(FirebaseInit.mAuth.getCurrentUser().getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(context, "Subcribed: "+categories.get(i).getTitle()+",index:"+i, Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(context, "Failure: "+categories.get(i).getTitle()+",index:"+i, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

        viewHolder.txtTitle.setText(categories.get(i).getTitle());
        return view;
    }

    public class ViewHolder {
        ImageView ivChannelAvatar;
        TextView txtTitle,txtId;
    }
}
