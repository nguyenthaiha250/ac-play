package com.android.c88.acplay.subscription;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Category;
import com.google.firebase.database.DatabaseError;

import java.util.List;

/**
 * Created by HiepLH2 on 12/9/2017.
 */

public class SuggestionViewModel extends AndroidViewModel {
    private static String TAG = "ACPlaySuggVM";
    private Repository mRepository;
    public ObservableList<Category> categories = new ObservableArrayList<>();


    public SuggestionViewModel(@NonNull Application application, Repository mRepository) {
        super(application);
        this.mRepository = mRepository;
    }
    public void start() {
        getCategories();
    }

    void getCategories() {
        mRepository.getSuggCategory(new DataSource.GetSuggCategoryCallback() {
            @Override
            public void onLoaded(List<Category> categories) {
                SuggestionViewModel.this.categories.clear();
                SuggestionViewModel.this.categories.addAll(categories);
                Toast.makeText(getApplication(), categories.size()+"  --++", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.e("getMovie", error.getMessage());
            }
        });
    }
    void subscribeChannel(String s){
        mRepository.subscribeChannel(new DataSource.SubscribeChannelCallback() {
            @Override
            public void onSuccess(Void aVoid) {
                getCategories();
            }

            @Override
            public void onFailture(Exception e) {

            }
        }, s);
    }
}
