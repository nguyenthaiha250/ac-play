package com.android.c88.acplay.trend;

import android.databinding.BindingAdapter;
import android.widget.ListView;

import com.android.c88.acplay.data.models.Movie;

import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class TrendBinding {
    @SuppressWarnings("unchecked")
    @BindingAdapter("app:trendItems")
    public static void setItems(ListView listView, List<Movie> items) {
        TrendAdapter adapter = (TrendAdapter) listView.getAdapter();
        if (adapter != null) {
            adapter.setMovies(items);
        }
    }
}
