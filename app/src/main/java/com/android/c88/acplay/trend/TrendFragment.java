package com.android.c88.acplay.trend;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.c88.acplay.ACCustomFragment;
import com.android.c88.acplay.ACMainActivity;
import com.android.c88.acplay.data.models.Movie;
import com.android.c88.acplay.databinding.FragmentTrendBinding;
import com.android.c88.acplay.media.VideoViewActivity;

import java.io.Serializable;
import java.util.ArrayList;


public class TrendFragment extends ACCustomFragment {

    private static String TAG = "TrendFragment";
   private FragmentTrendBinding mFragmentTrendBinding;
    private TrendViewModel mTrendViewModel;
    private TrendAdapter adapter;
    private Intent intent;

    private OnFragmentInteractionListener mListener;

    public TrendFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TrendFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TrendFragment newInstance() {
        TrendFragment fragment = new TrendFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTrendViewModel.start();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentTrendBinding = FragmentTrendBinding.inflate(inflater, container, false);
        mTrendViewModel = ACMainActivity.obtainTrendViewmodel(getActivity());
        mFragmentTrendBinding.setViewmodel(mTrendViewModel);
        adapter = new TrendAdapter(getContext(), new ArrayList<Movie>(), mTrendViewModel);
        adapter.notifyDataSetChanged();
        mFragmentTrendBinding.lvVideo.setAdapter(adapter);

        mFragmentTrendBinding.lvVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), mTrendViewModel.trendMovies.get(i).getTitle(), Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(),VideoViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoViewActivity.KEY_MOVIE,(Serializable) mTrendViewModel.trendMovies.get(i));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return mFragmentTrendBinding.getRoot();
       // return inflater.inflate(R.layout.fragment_trend, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
