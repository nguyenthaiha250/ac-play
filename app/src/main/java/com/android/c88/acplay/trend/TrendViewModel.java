package com.android.c88.acplay.trend;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.c88.acplay.data.DataSource;
import com.android.c88.acplay.data.Repository;
import com.android.c88.acplay.data.models.Movie;
import com.google.firebase.database.DatabaseError;

import java.util.Collections;
import java.util.List;

/**
 * Created by THAIHA on 12/11/2017.
 */

public class TrendViewModel extends AndroidViewModel{
    private static String TAG = "TrendViewModel";
    private Repository mRepository;
    public ObservableList<Movie> trendMovies = new ObservableArrayList<>();
    public TrendViewModel(@NonNull Application application,Repository repository) {
        super(application);
        mRepository = repository;
    }
    public void start() {
        getMovies();
    }

    void getMovies() {
        trendMovies.clear();
        mRepository.getTrend(new DataSource.GetTrendCallback() {
            @Override
            public void onLoaded(List<Movie> movies) {
                for (Movie m : movies) {
//                    Log.d(TAG, "con me no nhan dc ma k hien"+m.toString());
                }
                Collections.reverse(movies);
                TrendViewModel.this.trendMovies.addAll(movies);
            }

            @Override
            public void onCancel(DatabaseError error) {
                Log.e("getMovie", error.getMessage());
            }
        });
    }
}
