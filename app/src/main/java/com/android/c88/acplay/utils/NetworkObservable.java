package com.android.c88.acplay.utils;

import java.util.Observable;

/**
 * Created by HiepLH2 on 12/13/2017.
 */

public class NetworkObservable extends Observable {
    private boolean isConnected;

    public void setConnected(boolean connected) {
        synchronized (this) {
            isConnected = connected;
        }
        setChanged();
        notifyObservers();
    }

    public synchronized boolean isConnected() {
        return isConnected;
    }
}
